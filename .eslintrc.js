module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module'
  },
  extends: [
     'eslint:recommended',
     'plugin:ember/recommended',
     'plugin:ember-suave/recommended'
  ],
  env: {
    'browser': true
  },
  rules: {
    indent: ['error', 4],
    'space-before-function-paren': ['error', {anonymous: 'ignore', named: 'never'}],
    'object-curly-spacing': ['error', 'never'],
    'array-bracket-spacing': ['error', 'never'],
    'key-spacing': ['error', {mode: 'minimum'}],
    'ember/avoid-leaking-state-in-ember-objects': ['warning', [
        'classNames',
        'classNameBindings',
        'actions',
        'concatenatedProperties',
        'mergedProperties',
        'positionalParams',
        'attributeBindings',
        'queryParams',
        'attrs',

        'copyableOptions',
        'roles'
    ]],
    'ember/no-new-mixins': ['off'],
    'ember/routes-segments-snake-case': ['warning']
  },
  overrides: [
    // node files
    {
      files: [
        'testem.js',
        'ember-cli-build.js',
        'config/**/*.js',
        'lib/*/index.js',
        'tests/dummy/config/**/*.js'
      ],
      parserOptions: {
        sourceType: 'script',
        ecmaVersion: 2015
      },
      env: {
        browser: false,
        node: true
      }
    }
  ]
};
