import {helper} from '@ember/component/helper';

/**
 * Helper that simply returns the passed parameters as an array.
 *
 * Usage:
 *
 * ```handlebars
 * {{array "Item 1" "Item 2"}} <!-- outputs ["Item 1", "Item 2"] -->
 * ```
 * Parameters:
 *
 * Unlimited. You can put as many as you want to make in the array.
 * @class ArrayHelper
 * @extends Ember.Helper
 * @module Helpers
 */
export default helper(function(params) {
    return params;
});
