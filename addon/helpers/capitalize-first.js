import {helper} from '@ember/component/helper';
import {capitalize} from '@ember/string';

/**
 * Helper that capitalizes the first letter of a word
 *
 * Usage:
 *
 * ```handlebars
 * {{capitalize-first "word"}} <!-- outputs "Word" -->
 * ```
 * Parameters:
 * * word - the string word or phrase that you wish the first letter to be capitalized in
 * @class Capitalize-First
 * @extends Ember.Helper
 * @module Helpers
 */
export default helper(function ([word = '']) {
    return capitalize(word);
});
