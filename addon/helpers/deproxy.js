import ObjectProxy from '@ember/object/proxy';
import {helper} from '@ember/component/helper';

/**
 * This helper grabs the contents of an ember proxy, or returns the object if its not a proxy. If you are having difficulty accessing properties in an object because it is still a proxy,
 * deproxy is able to help you. This might be especially helpful in passing objects to components.
 *
 * ```handlebars
 * {{deproxy object}}
 * ```
 * Parameters:
 * * Object - object which may be a proxy.
 * @module Helpers
 * @class Deproxy
 * @extends Ember.Helper
 */
/**
 * Grabs the contents of an ember proxy, or returns the object if its not a proxy
 * @param  {object} object Object which may be a proxy
 * @return {object}        Proxy content, or object if not a proxy
 */
export function deproxy(object) {
    if (object instanceof ObjectProxy) {
        object = object.get('content');
    }
    return object;
}

/**
 * Handlebars helper version of {@link deproxy}
 * @param  {object} object Object which may be a proxy
 * @return {object}        Proxy content, or object if not a proxy
 */
export default helper(function([object]) {
    return deproxy(object);
});
