/* global moment */
import {helper} from '@ember/component/helper';
/**
* This formats a moment object into a date string according to the string format that is desired.
*
 * Usage:
 * ```handlebars
 * {{moment-format momentObject 'DD-MM-YYYY'}} <!--returns "01-01-1970"-->
 * ```
 * Parameters:
 * * momentObject - a moment object (either in a model with moment-date attr, or selected by bs-datetimepicker)
 * * format - This tells the helper how the date should be returned as a string.  For codes for the string format,
 *  [click here](https://momentjs.com/docs/#/displaying/format/) for Moment.js string format.
 * @class Moment-Format
 * @extends Ember.Helper
 * @module Helpers
 */

export default helper(function([momentObj, format]) {
    if (moment.isMoment(momentObj)) {
        return momentObj.format(format);
    }
    return null;
});
