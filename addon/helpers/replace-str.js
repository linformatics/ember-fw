import {helper} from '@ember/component/helper';
import {htmlSafe} from '@ember/string';

/**
* Replaces string. Works the same as any other programming language replace function
* Also is htmlSafe, so you can do replaces with html code as well.
*
 * Usage:
 * ```handlebars
 * {{replace-str 'Hello Illinois' 'Illinois' 'World'}} <!-- returns 'Hello World'-->
 * ```
 * Parameters:
 * * haystack - the string which will have data replaced from it
 * * needle - the string which you are planning to replace from the haystack string
 * * replace - the string that will be replacing the needle within the haystack string
 *
 * @class Replace-Str
 * @extends Ember.Helper
 * @module Helpers
 */

export default helper(function([haystack, needle, replace]) {
    return htmlSafe(haystack.replace(needle, replace));
});
