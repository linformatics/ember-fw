import Route from '@ember/routing/route';
import {inject} from '@ember/service';
import {isEmpty} from '@ember/utils';

/**
 * Base route that handles html computed title modification in particular routes
 *
 * Usage: (in a routes file)
 * ```javascript
 * import BaseRoute from '@bennerinformatics/ember-fw/routes/base';
 *
 * export default BaseRoute.extend({
 *     // Your code here
 * });
 * ```
 *
 * @class BaseRoute
 * @extends Ember.Route
 * @module Miscellaneous
 */
export default Route.extend({
    config: inject(),

    /**
     * Numer of times to retry the model if it returns a 500 error
     * @property _retries
     * @type {Number}
     */
    _retries: 2,

    /**
     * Called to update the page's title from the current title
     * @method updateTitle
     */
    updateTitle() {
        let appName = this.get('config.name');
        let title = this.get('title');
        let fullTitle = (isEmpty(title)) ? appName : `${title} | ${appName}`;
        document.title = fullTitle;
    },

    actions: {
        /**
         * This sets the title to the updated title, and adds an observer to change incase the title ever changes
         * @method didTransition
         */
        didTransition() {
            this._super(...arguments);

            // just set the title for now, and add an observer to update it later
            this.updateTitle();
            this.addObserver('title', this, 'updateTitle');
        },
        /**
         * This removes the observer to the updateTitle
         * @method willTransition
         */
        willTransition() {
            this._super(...arguments);

            this.removeObserver('title', 'updateTitle');
        },
        /**
         * Error method catches an error and will retry the error if it is a 500 error. After a certain amount of retries, it will just return a boolean "true" saying it did error.
         * @method error
         * @param  {Object} error      This is the error object that is passed when an error is thrown
         * @param  {Object} transition This is the transition object that errored.
         * @return {Boolean}
         */
        error(error, transition) {
            // 500 errors
            if ((error.status / 100) === 5 && this._retries > 0) {
                this._retries--;
                transition.retry();
                return false;
            }

            return true;
        }
    }
});
