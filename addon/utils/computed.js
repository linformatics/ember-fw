import {isArray} from '@ember/array';
import {computed} from '@ember/object';
import {isEmpty, isNone} from '@ember/utils';

/**
 * This Util is all about making common computed properties easier to use. It took several very common computed properties that are used throughout the apps and made them exportable,
 * so each of them could be used with less code. Each returns a computed property for you, and will function the same as if you had written the simple computed property yourself.
 *  Watch properties will also automatically be done for you. Each of the "methods" in this documentation are actually different computed properties you are able to import.
 *
 * Import syntax:
 * ```javascript
 * import {functionName} from '@bennerinformatics/ember-fw/utils/computed';
 *
 * //where functionName is the name of the function, so for example, the "sortBy" function would have the following syntax
 * import {sortBy} from '@bennerinformatics/ember-fw/utils/computed';
 * ```
 *
 * @module Utils
 * @class ComputedUtil
 */
/**
 * Filters an array by a list of keys, requiring every one to match. If you wish to filter by only one key, check out
 * [Ember's filterBy util](https://api.emberjs.com/ember/release/functions/@ember%2Fobject%2Fcomputed/filterBy).
 *
 * Usage example:
 * ```javascript
 * filteredLocations: filterByKeys('sortedLocations', ['active', 'inDept']),
 * ```
 *
 * @method filterByKeys
 * @param  {String} array Key of the array (important this is the NAME of the array property, not the actual array itself)
 * @param  {Array}  keys  Array of keys to check, must all be truthy
 * @return {computed}     computed property
 */
export function filterByKeys(array, keys) {
    return computed(`${array}.@each.{${keys.join(',')}}`, function() {
        let data = this.get(array);
        if (isEmpty(data)) {
            return [];
        }
        return data.filter((item) => {
            return keys.every((key) => item.get(key));
        });
    });
}

/**
 * Computed macro that redirects to array sortBy. It can take one key or multiple, and it essentially sorts the array for you with anything that is able to be used by the sortBy method.
 * Unfortunately since the vanilla sortBy function does not take 'key:asc' or 'key:desc', there needed to be a workaround for sorting in desc order. Unfortunately, this workaround does not allow
 * you to sort one key desc and one key asc, but it will allow you to reverse the order of the array.
 *
 * Usage Example:
 * ```javascript
 * //sort ascending model.locations by two keys, first order, then title
 * sortedLocations: sortBy('model.locations', ['order', 'title']),
 *
 * //sort ascending model.users by one key, name
 * sortedUsers: sortBy('model.users', 'nameFull')
 *
 * //sort descending model.entries by one key, date._d
 * sortedEntries: sortBy('model.entries', 'date._d', true),
 * ```
 * @method sortBy
 * @param  {String} array Name of array to sort
 * @param  {String|array} keys  Key or list of keys to sort by
 * @param {Boolean} reverse Optional param to sort the items in descending order. Default is false
 * @return {computed}       Computed sort property
 */
export function sortBy(array, keys, reverse = false) {
    let watch;
    if (isArray(keys)) {
        watch = `${array}.@each.{${keys.join(',')}}`;
    } else {
        watch = `${array}.@each.${keys}`;
        keys = [keys];
    }

    return computed(watch, function() {
        let data = this.get(array);
        if (!isArray(data) || isEmpty(array)) {
            return data;
        }
        if (reverse) {
            return data.sortBy(...keys).reverse();
        }
        return data.sortBy(...keys);
    });
}

/**
 * Computed property that returns a call to store.peekAll. Will update accordingly. Note that this is a peekAll request not a findAll request, if you do not know the difference,
 * see [Ember Guides](https://guides.emberjs.com/v2.18.0/models/finding-records/) on the differences between the two.
 *
 * Usage Example:
 * ```javascript
 * users: storePeekAll('user'),
 * ```
 * @method storePeekAll
 * @param  {String} model Model name to peek
 * @return {computed}     Computed property
 */
export function storePeekAll(model) {
    return computed('store', function() {
        let store = this.get('store');
        if (isNone(store)) {
            return [];
        }
        return store.peekAll(model);
    });
}
