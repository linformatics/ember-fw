import {get} from '@ember/object';
import {isNone} from '@ember/utils';

/**
 * This Util is all about making clearer and easy to define error messages from server side errors. Most notably, it automatically takes an error and formats it
 * in such a way to make the message make sense, but it also has a download link to download the whole stack trace of the error if the error cannot properly fit
 * onto a one line error message. This should be used anytime that there is an error from a serverside because otherwise, the link will likely not be setup properly.
 * As a suggestion modals should use the `createErrorMessage` and non-modals should probably use `handleAjaxError`. Each of the "methods" in this documentation are
 * actually different functionss you are able to import, but beware that the only ones you should probably be using are the non-private functions unless you have a very
 * good reason for using a private function.
 *
 * Import syntax:
 * ```javascript
 * import {functionName} from '@bennerinformatics/ember-fw/utils/error';
 *
 * //where functionName is the name of the function, so for example, the "createErrorMessage" function would have the following syntax
 * import {createErrorMessage} from '@bennerinformatics/ember-fw/utils/error';
 * ```
 *
 * @module Utils
 * @class ErrorUtil
 */

/**
 * Gets the title to display for an error object using both the status and the error message. This is primarily an internal function used by both createErrorMessage and
 * handleAjaxError.
 * @method getTitle
 * @param  {error}  error Error object
 * @return {string}       String title
 * @private
 */
function getTitle(error) {
    let message = get(error, 'payload.message') || error.message || 'An unknown error occured';
    return `${error.status} Error - ${message}`;
}

/**
 * Converts an error object into a link. This is primarily an internal function used by createErrorMessage and handleAjaxError.
 * @method createErrorLink
 * @param  {error}  error Error object
 * @param  {string} text  Clickable link text
 * @return {string}       Clickable URL
 * @private
 */
export function createErrorLink(error, text) {
    let title = getTitle(error);
    let {payload} = error;
    // return objects as JSON
    if (typeof(payload) === 'object') {
        return `<a href="data:text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(payload))}" download="${title}.json">${text}</a>`;
    }
    return `<a href="data:text/plain;charset=utf-8,${encodeURIComponent(payload)}" download="${title}.txt">${text}</a>`;
}

/**
 * Creates a full error message based on the given error payload. This function should be used in contexts when the outlet is not `fixed`.
 *
 * Usage Example:
 * ```javascript
 * promise.catch((error) => {
 *   this.get('notifications').showError(createErrorMessage(error), 'modal-top', true);
 * });
 * ```
 * @method createErrorMessage
 * @param  {error}  error Error object
 * @return {string}       Notification error message
 */
export function createErrorMessage(error) {
    if (typeof(error) === 'object') {
        let message = getTitle(error);
        if (!isNone(error.payload)) {
            message += ` (${createErrorLink(error, 'more details')})`;
        }
        return message;
    }
    return error ? error.toString() : 'An unknown error occured';
}

/**
 * All in one logic to handle an ajax error message. This also will handle the error and place it in the
 * fixed outlet only, so if the error message is to go to the fixed outlet, use this, otherwise use createErrorMessage.
 * Requires notifications to be injected into the calling controller/component and being bound to that object.
 *
 * Usage Example:
 * ```javascript
 * promise.catch(handleAjaxError.bind(this));
 * ```
 * @method handleAjaxError
 * @param  {error}  error Error object
 */
export function handleAjaxError(error) {
    this.get('notifications').showError(createErrorMessage(error), 'fixed', true);
    throw error;
}
