import {computed} from '@ember/object';
import {isEmpty, isNone} from '@ember/utils';
import moment from 'moment';

/**
 * The two-way-binding util is all about making query parameters that are bound to their objects that they describe. For example if you desire to have a model within a query parameter,
 * you need to make sure that you only have the id in the url, but actually have access to the full model that belongs to that id within the controller. Two way bindings makes
 * this process much easier. For more information on query parameters in general, [click here](https://guides.emberjs.com/v2.18.0/routing/query-params/). These two way bindings are specfically designed
 * for moment objects or Ember models. Note: this function will go on the "id" field being displayed in the URL, and if either key or model is changed, both are changed properly. Each of the "methods" in this
 * documentation are actually different computed properties you are able to import.
 *
 * Import Syntax:
 * ```javascript
 * import {functionName} from '@bennerinformatics/ember-fw/utils/two-way-bindings';
 *
 * //where functionName is the name of the function, so for example, the "momentToString" function would have the following syntax
 * import {momentToString} from '@bennerinformatics/ember-fw/utils/computed';
 * ```
 * @module Utils
 * @class TwoWayBindingUtil
 */
/**
 * Two way binding between a moment object and a query param string. This goes on the query parameter to show the date in the url.
 *
 * Usage Example:
 * ```javascript
 * //actual moment object set by bs-datetimepicker or something similar
 * start: null,
 *
 * //moment string to be displayed in the url
 * startString: momentToString('start', 'YYYY-MM-DD'),
 * ```
 * @method momentToString
 * @param  {String} momentKey                       Key of the moment object
 * @param  {String} [format='YYYY-MM-DD HH:mm:ss']  Format to use for the moment string
 * @return {Computed}                               Computed property
 */
export function momentToString(momentKey, format = 'YYYY-MM-DD HH:mm:ss') {
    return computed(momentKey, {
        get() {
            let m = this.get(momentKey);
            if (moment.isMoment(m)) {
                return m.format(format);
            }
            return null;
        },
        set(key, value) {
            if (isNone(value)) {
                return null;
            }

            let m = moment(value, format);
            if (!moment.isMoment(m)) {
                return null;
            }

            this.set(momentKey, m);
            return value;
        }
    });
}

/**
 * Two way binding between a model and a value in a model array. Set this function to the query parameter value:
 *
 * Usage Example:
 * ```javascript
 * //the actual model, which is set as the "selected" attribute of the power-select or something similar
 * locationModel: null,
 *
 * //note: model.locations is the array of location models, which is set as the "options" attribute of the power-select or something similar
 * //the model id, which will be used in the url
 * locationId: modelToId('locationModel', 'model.locations')
 * ```
 * @method modelToId
 * @param  {String} modelKey   Key of the model itself
 * @param  {String} optionsKey Key of the model options array (ie the full list of models that are the options for the power select)
 * @return {Computed}          Computed property
 */
export function modelToId(modelKey, optionsKey) {
    return computed(modelKey, {
        get() {
            return this.get(`${modelKey}.id`);
        },
        set(key, value) {
            // skip if null
            if (isNone(value)) {
                return null;
            }
            // ensure we have options
            let options = this.get(optionsKey);
            if (isEmpty(options)) {
                return value;
            }
            let newModel = options.findBy('id', value);
            // if we cannot find it, return null
            if (isNone(newModel)) {
                return null;
            }

            // found one? set it and return the ID
            this.set(modelKey, newModel);
            return value;
        }
    });
}

/**
 * Two-way binding between an array of model IDs and an array of model objects.
 *
 * Usage Example:
 * ```javascript
 * // The actual model array, which is set as the "selected" attribute of the power-select-multiple or something similar
 * selectedModels: [],
 *
 * // The array of model IDs, which will be used in the URL
 * selectedModelIds: modelsToIds('selectedModels', 'model.options')
 * ```
 * @method modelsToIds
 * @param  {String} modelsKey   Key of the model array
 * @param  {String} optionsKey  Key of the model options array (the full list of models that are the options for the power-select-multiple)
 * @return {Computed}           Computed property
 */
export function modelsToIds(modelsKey, optionsKey) {
    return computed(`${modelsKey}.[]`, {
        get() {
            let models = this.get(modelsKey);
            if (isEmpty(models)) {
                return [];
            }
            return models.map((model) => model.id);
        },
        set(key, values) {
            if (isNone(values)) {
                return [];
            }
            // Ensure we have options
            let options = this.get(optionsKey);
            if (isEmpty(options)) {
                return values;
            }
            let newModels = values.map((value) => options.findBy('id', value)).filter((model) => !isNone(model));
            this.set(modelsKey, newModels);
            return values;
        }
    });
}