import {inject as service} from '@ember/service';
import {alias} from '@ember/object/computed';
import RESTAdapter from 'ember-data/adapters/rest';
import AjaxSupportMixin from 'ember-ajax/mixins/ajax-support';

/**
 * The fw adapter defines a base adapter for the ember-data store.
 * By default, this is included in an app as the `application` adapter; however,
 * if you should need to override/extend any behavior in your particular app,
 * you may extend this in another adapter.
 *
 * For example:
 *
 * ```javascript
 * import FwAdapter from '@bennerinformatics/ember-fw/adapters/fw';
 *
 * export default FwAdapter.extend({
 *     // ... your code here
 * });
 * ```
 *
 * Note: If you just want to extend/override the adapter for one particular
 * model, you can do so by providing an adapter for that particular model
 * without overriding the base one.
 *
 * For example, if you had a 'user' model:
 *
 * ```javascript
 * import BaseAdapter from './application';
 *
 * export default BaseAdapter.extend({
 *     // ... your code here
 * });
 * ```
 *
 * @class FwAdapter
 * @extends DS.RESTAdapter
 * @module Miscellaneous
 */
export default RESTAdapter.extend(AjaxSupportMixin, {

    /**
     * The `config` property is an injected service that provides
     * various properties such as the api root that are used to handle
     * requests.
     *
     * @property config
     * @type ConfigService
     */
    config: service(),

    /**
     * Computed alias of `config.apiRoot`
     *
     * @property host
     * @type String
     */
    host: alias('config.apiRoot'),

    /**
     * Ensures trailing slashes for all urls (extends Ember-Data's method)
     *
     * @method buildURL
     * @return String
     */
    buildURL() {
        // Ensure trailing slashes
        let url = this._super(...arguments);

        if (url.slice(-1) !== '/') {
            url += '/';
        }

        return url;
    },

    shouldBackgroundReloadRecord() {
        return false;
    }
});
