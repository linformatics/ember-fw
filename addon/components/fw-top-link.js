import LinkComponent from '@ember/routing/link-component';

/**
 * Same as `LinkTo`, excepts scrolls to the top of the page when clicking the link. For full documentation on Ember's `LinkTo` component, please [click here](https://api.emberjs.com/ember/3.4/classes/Ember.Templates.helpers/methods/link-to?anchor=link-to)
 *
 * Used primarily in `FwHeader` and `FwNav` as those links can be clicked further down the page.
 *
 * @class FwTopLink
 * @extends LinkComponent
 * @module Components
 */
export default LinkComponent.extend({
    _scrollToTop() {
        window.scrollTo({top: 0, behavior: 'smooth'});
    },

    init() {
        this._super(...arguments);

        this.on(this.get('eventName'), this, this._scrollToTop);
    },

    willDestroyElement() {
        this._super(...arguments);

        this.off(this.get('eventName'), this, this._scrollToTop);
    }
});
