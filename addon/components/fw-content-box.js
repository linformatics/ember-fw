import Component from '@ember/component';
import {computed} from '@ember/object';
import layout from '@bennerinformatics/ember-fw/templates/components/fw-content-box';

/**
 * This component is used to generate a basic bootstrap content-box (or panel). Content-boxes
 * can have a header, footer, and a body. These individual parts can be disabled optionally.
 *
 * The component passes in a `section` variable for use in determining what
 * content goes in what part of the box. This `section` variable will have one of three
 * different values: "header", "body", or "footer".
 *
 * Basic Usage:
 *
 * ```handlebars
 * <FwContentBox as |section|>
 *     {{#if section.header}}
 *         <p>Header Content</p>
 *     {{else if section.body}}
 *         <p>Body Content</p>
 *     {{else if section.footer}}
 *         <p>Footer Content</p>
 *     {{/if}}
 * </FwContentBox>
 * ```
 *
 * To disable individual sections, you may modify the hasHeader, hasFooter, or
 * hasBody attributes of the component:
 *
 * ```hbs
 * <FwContentBox @hasHeader={{false}} as |section|>
 *     {{!same as other box}}
 * </FwContentBox>
 * ```
 *
 * This is useful if you don't have any content to show in a certain part of the
 * box, or you want to hide different parts based on permissions. values, etc.
 *
 * @class FwContentBox
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    layout,
    classNames: 'panel',
    classNameBindings: ['panelClass'],
    /**
     * The panel class should not to be changed by the user using the component, but it computes the type defined by the user and changes it to the panel bootstrap class
     * which is needed.
     * @property panelClass
     * @type {Computed Property}
     * @return {String} Panel Class (Bootstrap)
     * @private
     */
    panelClass: computed('type', function () {
        let type = this.get('type');

        return `panel-${type}`.htmlSafe();
    }),

    /**
     * The `type` is used to define the panel type. Can be 'default', 'primary',
     * 'success', 'danger', 'info', etc. See [bootstrap docs](https://mdbootstrap.com/docs/b4/jquery/components/panels/) for more details.
     * @property type
     * @type {String}
     * @default 'default'
     */
    type: 'default',

    /**
     * Used to determine whether or not the header of the content box should be rendered.
     * @property hasHeader
     * @type {Boolean}
     * @default true
     */
    hasHeader: true,

    /**
     * Used to determine whether or not the body of the content box should be rendered.
     * @property hasBody
     * @type {Boolean}
     * @default true
     */
    hasBody: true,

    /**
     * Used to determine whether or not the footer of the content box should be rendered.
     * @property hasFooter
     * @type {Boolean}
     * @default true
     */
    hasFooter: true,

    /**
     * Adds the overflow-x: auto property to the body of the content-box
     * in the case that fixed width content is greater than the width of the box.
     *
     * @property scrollable
     * @type {Boolean}
     * @default true
     */
    scrollable: false
});
