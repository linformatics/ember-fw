import Component from '@ember/component';
import {computed} from '@ember/object';
import {alias} from '@ember/object/computed';
import {inject as service} from '@ember/service';
import layout from '@bennerinformatics/ember-fw/templates/components/fw-notification';

/* All events we subscribe to for the sake of the event listeners */
const EVENTS = ['animationend', 'webkitAnimationEnd', 'oanimationend', 'MSAnimationEnd'];

/**
 * Internally used with the [Notifications](FwNotifications.html) component. For the most part, you should not be using this component, but rather defining the outlet using fw-notifications.
 *
 * @class FwNotification
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    layout,
    tagName: 'article',
    classNames: ['alert', 'alert-dismissable'],
    classNameBindings: ['colorClass', 'dismissable:fw-alert-passive'],
    /**
     * The colorClass computes the alert class based on the type passed into the notification.
     * @property colorClass
     * @type Computed
     * @private
     */
    colorClass: computed('message.type', function () {
        return `alert-${this.get('message.type')}`;
    }),

    /**
     * Dismissable is just an alias of message.dismissable.
     * @property dismissable
     * @type {Boolean}
     * @private
     */
    dismissable: alias('message.dismissable'),

    // add the style attribute to change max height
    attributeBindings: ['style'],

    /**
     * Style is an attribute binding that is used to add animation delay to the style on the notification html element. It is set to the message.length if both message.length and message.dissmissable are defined.
     * @property style
     * @type Computed
     * @private
     */
    style: computed('message.length', function() {
        if (this.get('message.length') && this.get('message.dismissable')) {
            return `animation-delay: ${this.get('message.length')};`;
        }
        return null;
    }),

    /**
     * Message is actually the object passed in with all the settings for the notification through the notifications service.
     * @property message
     * @type {Object}
     */
    message: null,
    _listener: null,

    notifications: service(),

    didInsertElement() {
        this._super(...arguments);

        let listener = (event) => {
            if (event.animationName === 'fade-out') {
                this.get('notifications').closeNotification(this.get('message'));
            }
        };
        this.set('_listener', listener);
        EVENTS.forEach((name) => this.element.addEventListener(name, listener));
    },

    willDestroyElement() {
        this._super(...arguments);
        let listener = this.get('_listener');
        EVENTS.forEach((name) => this.element.removeEventListener(name, listener));
    },

    actions: {
        closeNotification() {
            this.get('notifications').closeNotification(this.get('message'));
        }
    }
});
