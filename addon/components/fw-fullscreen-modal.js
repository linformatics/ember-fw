import layout from '@bennerinformatics/ember-fw/templates/components/fw-fullscreen-modal';
import Component from '@ember/component';
import {computed} from '@ember/object';
import RSVP from 'rsvp';

/**
 * Component that handles rendering of modals. To use this you must define
 * a component that extends the BaseModal component
 * and call this component with the following basic usage:
 *
 * ```handlebars
 * <FwFullscreenModal @modal="modal-name" @size="md" @close={{action 'closeModal'}} @model={{model}} />
 * ```
 *
 * Alternatively, if it is a very simple modal, it is possible to pass in a yield block and circumvent creating a modal component file (with this use the model attribute and modal attribute do not work). That would be with the following usage:
 * ```handlebars
 * <FwFullscreenModal @size="md" @close={{action 'closeModal'}}>
 *    {{!modal content here}}
 * </FwFullscreenModal>
 * ```
 * Note: Once this component appears in the DOM it will be rendered and shown,
 * so for most usage you should wrap it in an `{{if}}` helper and render it conditionally
 *
 * ```handlebars
 * {{#if shouldRenderModal}}
 *     <FwFullScreenModal @modal="modal-name" @size="md" ... />
 * {{/if}}
 * ```
 *
 * When shouldRenderModal is `true`, the modal will appear, otherwise it will stay hidden. So the action to "open" the modal should within it set the variable shouldRenderModal to true, and the
 * action to "close" the modal should set shouldRenderModal to false.
 *
 * @class FwFullscreenModal
 * @extends LiquidTetherComponent
 * @module Components
 *
 */
const FullScreenModalComponent = Component.extend({
    layout,

    /**
     * Action called on close. This is passed to the modal component as closeModal, so it can be accessed there under that name.
     * @property close
     * @type {Action}
     */
    close: () => RSVP.resolve(),

    /**
     * Action called on modal confirm.
     * @property confirm
     * @type {Action}
     */
    confirm: () => RSVP.resolve(),

    /**
     * Modal component name. If there is not a file located in components/modals folder, it will throw an error. If using the block form of this component, this attribute is unnecessary.
     * @property modal
     * @type {String}
     */

    /**
     * This is a required element that passes data to the modal. This model property is accessible in the modal component as the variable "model". If multiple values are needed, it can be used with the [hash](https://api.emberjs.com/ember/3.12/classes/Ember.Templates.helpers/methods/hash?anchor=hash) helper to combine multiple values to the model. If using the block form with the yield, this parameter will not work.
     * @property model
     * @type {Dynamic}
     */

    /**
     * Path to the currently rendered modal component. This is set automatically by the init function.
     *
     * @protected
     * @property modalPath
     * @type {String}
     * @default 'unknown'
     */
    modalPath: 'unknown',

    /**
     * Size of the modal. The size available are 'xs' (maxwidth is set to 300px), 'sm' (maxwidth is 600px), 'md' (maxwidth is 900px), 'lg' (maxwidth is 1200px), 'xl' (maxwidth is 1500px) and 'full' (maxwidth is 95vw). Default is used if not set or if option not allowed is used
     * @default 'md'
     * @property size
     * @type {String}
     */
    size: 'md',

    /**
     * Whether or not the modal should be able to be closed by clicking on the
     * background around it. Set this to be false if you do not want people to
     * be able to click out of the modal without prompting them first.
     * @default false
     * @property closeOnClick
     * @type {Boolean}
     */
    closeOnClick: false,

    /**
     * Whether or not the browser will scroll to the top of the page when this modal opens.
     *
     * @default false
     * @property scrollTop
     * @type {Boolean}
     */
    scrollTop: false,

    /**
     * If true the browser will scroll to the top of the screen while the modal is open and then return to where it was when the modal is closed again. scrollTop must be set to false for this to work properly.
     *
     * @default false
     * @property scrollTopReturn
     * @type {Boolean}
     */
    scrollTopReturn: false,

    /**
     * Hides the scrollbar for modals
     *
     * @default true
     * @property hideBackgroundScroll
     * @type {Boolean}
     */
    hideBackgroundScroll: true,
    /**
     * Initializes the component and loads the modal path.
     *
     * @private
     * @method init
     */
    init() {
        this._super(...arguments);
        this.set('modalPath', `modals/${this.get('modal')}`);
    },

    /**
     * After the component's element has been inserted into the DOM, it deals with various config settings that were passed in: scrollTop, scrollTopReturn, and hideBackgroundScroll.
     *
     * @private
     * @method didInsertElement
     */
    didInsertElement() {
        this._super(...arguments);
        if (this.get('scrollTop')) {
            window.scrollTo({top: 0, behavior: 'smooth'});
        }

        if (this.get('scrollTopReturn')) {
            this.set('scrollY', window.scrollY);
            window.scrollTo({top: 0});
        }

        if (this.get('hideBackgroundScroll')) {
            document.querySelector('body.ember-application').style.overflow = 'hidden';
        }
    },
    /**
     * Once the component is being exited, it returns the scrollbar to the proper settings if scrollTopReturn is set.
     *
     * @private
     * @method willDestroyElement
     */
    willDestroyElement() {
        this._super(...arguments);
        if (this.get('scrollTopReturn')) {
            window.scrollTo({top: this.get('scrollY')});
        }

        if (this.get('hideBackgroundScroll')) {
            document.querySelector('body.ember-application').style.overflow = 'auto';
        }
    },

    /**
     * Returns the proper class based on the size attribute that was passed in. Watches: size.
     * @private
     * @property sizeClass
     * @type {String}
     */
    sizeClass: computed('size', function () {
        return `fullscreen-modal fullscreen-modal-${this.get('size')}`;
    }),

    actions: {
        close() {
            return this.close(...arguments);
        },

        confirm() {
            return this.confirm();
        },

        clickOverlay(event) {
            if (this.get('closeOnClick') && event.target.classList.contains('fullscreen-modal-container')) {
                this.close();
            }
        }
    }
});

FullScreenModalComponent.reopenClass({
    positionalParams: ['modal']
});

export default FullScreenModalComponent;
