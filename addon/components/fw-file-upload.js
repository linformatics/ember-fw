import {A} from '@ember/array';
import Component from '@ember/component';
import {isEmpty} from '@ember/utils';
import layout from '../templates/components/fw-file-upload';

/**
 * Creates a file upload component that more closely resembles the normal inputs.
 *
 * This is used as HTML does not allow styling on file uploads, so wrapping and hiding the button is the only solution. This is mostly used for single file uploads (though there is an option for multiple here).
 * Most apps use dropzone for multiple file uploads, [click here](https://github.com/FutoRicky/ember-cli-dropzonejs#readme) for more details.
 *
 * Basic Usage:
 * ```handlebars
 * <FwFileUpload @accept='.csv' @update={{action (mut file)}} />
 * ```
 * @class FwFileUpload
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    layout,

    tagName: 'label',
    classNames: ['input-group'],

    /**
     * Valid file extensions, for multiple separate with commas.
     * See the [HTML property](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/file#accept) for more information.
     * @property accept
     * @type {String}
     * @default null
     */
    accept: null,

    /**
     * Called when a file is uploaded. This is the function that actually does whatever is necessary with the file (aka, this is a mandatory property). Passed in as an action
     * @property update
     * @type {Action}
     * @param  {array|file} type File uploaded. If multiple, will be an array of files
     */
    update: () => {},

    /**
     * If true, makes this a multiple upload.
     * @property multiple
     * @type {Boolean}
     * @default false
     */
    multiple: false,

    /**
     * Maximum number of filenames to display in the input field
     * @property maxDisplay
     * @type {Number}
     * @default 3
     */
    maxDisplay: 3,

    /**
     * Text to display when no file is set.
     * @property noFileText
     * @type {String}
     * @default 'No file chosen'
     */
    noFileText: 'No file chosen',

    /**
     * File description from upload
     * @property _filename
     * @type {String}
     * @private
     */
    _filename: '',

    didReceiveAttrs() {
        this._super(...arguments);
        this.set('_filename', this.get('noFileText'));
    },

    /**
     * Normalizes the files to a practical array structure
     * @method normalizeFiles
     * @param  {FileList} files JavaScript filelist object
     * @return {File|array} File, or array of files if multiple
     */
    normalizeFiles(files) {
        // multiple gets an array, single gets the first item or null
        if (this.get('multiple')) {
            return A(Array.from(files));
        }
        if (!isEmpty(files)) {
            return files[0];
        }
        return null;
    },

    /**
     * Gets the filename from a list of files
     * @method getFileName
     * @param  {array|file} files list of files or single file if not multiple
     * @return  Filename to display
     */
    getFilename(files) {
        if (isEmpty(files)) {
            return this.get('noFileText');
        }

        if (this.get('multiple')) {
            if (files.get('length') > this.get('maxDisplay')) {
                return `${length} files`;
            }
            return files.map(({name}) => `"${name}"`).join(', ');
        }

        return files.name;
    },

    actions: {
        updateFile(event) {
            let files = this.normalizeFiles(event.currentTarget.files);
            this.set('_filename', this.getFilename(files));
            this.update(files);
        }
    }
});
