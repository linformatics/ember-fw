import Component from '@ember/component';
import {not} from '@ember/object/computed';
import layout from '../templates/components/fw-info-icon';

/**
 * Standard styling for popover icons used in the apps. By default, it displays an i circle and upon hovering over it, it displays the message that you set to it. Most of the time, it is used very simply, in the following way:
 *```handlebars
 * <FwInfoIcon @info="My special message to be displayed">
 * ```
 * The string sets the positional parameter, info. A positional parameter does not require a title, but determines the parameter based on the position of the value. In this case, FwInfoIcon assumes that the first parameter, if it is unnamed, is the info attribute.
 *
 * Note: This component extends the FwPopoverIcon component, but this icon has the normal styling, which that does not. More information on that can be found [here](FwPopoverIcon.html).
 *
 * @class FwInfoIcon
 * @extends Ember.Component
 * @module Components
 */
const InfoIcon = Component.extend({
    layout,
    // attributes
    tagName: 'a',
    classNames: ['icon', 'fw-info-icon'],
    classNameBindings: ['hidden', 'classes'],
    classes: null,
    /**
     * This property is the main property that is required to make this component work. It sets the content for what will be displayed in the icon message.
     * @property info
     * @type {String}
     * @default null
     */
    info: null,
    /**
     * Determines where the message bubble will appear in relation to the icon. Options: top, bottom, left, right, or auto. Auto is top or bottom depending on screen placement.
     * @property placement
     * @type {String}
     * @default 'auto'
     */
    placement: 'auto',

    /**
     * Determines how the message bubble will trigger. Options: hover, focus, click, and pinnable. Pinnable supports both hover and click.
     * @property popoverTrigger
     * @type {String}
     * @default 'pinnable'
     */
    popoverTrigger: 'pinnable',
    /**
     * This is the font awesome icon that will be displayed. It can be overridden by changing this property. It must have the font awesome prefix in order to work properly.
     * @property icon
     * @type {String}
     * @default 'fa-solid fa-circle-info'
     */
    icon: 'fa-solid fa-circle-info',

    // Doesn't actually appear to do anything. Not taking it out yet in case it does, but not documenting it either.
    isHtml: true,

    /**
     * Container for the popover, set to 'body' to prevent parent styling from modifying the popover.
     * @property container
     * @type {String}
     * @default null
     */
    container: 'body',

    /**
     * This will add the hidden class if info is null.
     * @private
     * @property hidden
     * @type {Boolean}
     */
    hidden: not('info')
});

InfoIcon.reopenClass({
    positionalParams: ['info']
});

export default InfoIcon;
