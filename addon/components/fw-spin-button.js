import Component from '@ember/component';
import {computed} from '@ember/object';
import {and} from '@ember/object/computed';
import {observer} from '@ember/object';
import {later, cancel} from '@ember/runloop';
import layout from '@bennerinformatics/ember-fw/templates/components/fw-spin-button';
import {htmlSafe} from '@ember/string';

/**
 * Borrowed from Ghost's 'gh-spin-button' component
 * Copyright 2013-2016 Ghost Foundation
 *
 * Button that spins to show something is happening.
 *
 * @class FwSpinButton
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    layout,
    tagName: 'button',
    classNameBindings: ['isProgress:progress-button', 'showSpinner:btn-spinning'],

    isProgress: and('showSpinner', 'progress'),

    /**
     * Action to call when clicking the button
     *
     * @property action
     * @type {Action}
     */
    action: () => {},

    /**
     * If you are rendering the promise button as an inline component and not
     * in block form, set this to whatever text you want the button to display.
     *
     * @property buttonText
     * @type {String}
     * @default ''
     */
    buttonText: '',

    /**
     * If you want a button-icon to be displayed, put any of the font-awesome icons
     * as a string here. Must use full icon name, including the class
     *
     * @property buttonIcon
     * @type {String}
     * @default ''
     */
    buttonIcon: '',

    /**
     * Title to display on button hover
     *
     * @property buttonTitle
     * @type {String}
     * @default null
     */
    buttonTitle: null,

    /**
     * Sets the icon to be shown when the promise is resolving. See
     * [here](http://fontawesome.io/examples/#animated) for a list
     * of possible icons.
     *
     * @property spinIcon
     * @type {String}
     * @default 'fa-solid fa-rotate'
     */
    spinIcon: 'fa-solid fa-rotate',

    /**
     * Variable used to specify whether the button should be spinning or not.
     *
     * @property submitting
     * @type {Boolean}
     * @default false
     */
    submitting: false,

    /**
     * Private instance variable holding whether or not the button is currently
     * spinning.
     *
     * @private
     * @property showSpinner
     * @type {Boolean}
     * @default false
     */
    showSpinner: false,

    /**
     * Private variable that holds the timer event that manages the
     * switch back to not spinning after a `timeout` period.
     *
     * @private
     * @property showSpinnerTimeout
     * @type {Event}
     * @default null
     */
    showSpinnerTimeout: null,

    /**
     * Specifies whether or not the button should retain its width
     * when the spinner is activated.
     *
     * @property autoWidth
     * @type {Boolean}
     * @default true
     */
    autoWidth: true,

    /**
     * If true, the button will be a fixed width and the icon will hide regardless of whether or
     * not the spinner is activated. (Used with {{#crossLink PromiseButton}}{{/crossLink}})
     *
     * @property fixWidth
     * @type {Boolean}
     * @default false
     */
    fixWidth: false,

    /**
     * Boolean property that disables the button when set to true.
     *
     * @property disableWhen
     * @type {Boolean}
     * @default false
     */
    disableWhen: false,

    /**
     * Sets the timeout of the button. If this is set, the button will
     * continue to spin for the time specified even after submitting is set to false.
     * This is useful if you wish to show that something happened, even if it happens
     * quickly.
     *
     * @property timeout
     * @type {Number}
     * @default 1000
     */
    timeout: 1000,

    /**
     * Sets the progress percent of the button. Used with submitting to show a sort of progress bar.
     * Must be a number between 0 and 100, setting to null disables it.
     *
     * @property timeout
     * @type {Number}
     * @default null
     */
    progress: null,

    // Disable button when button is spinning
    attributeBindings: ['disabled', 'type', 'tabindex', 'buttonTitle:title'],

    disabled: computed('disableWhen', 'showSpinner', function () {
        return this.get('disableWhen') || (this.get('showSpinner') === true);
    }),

    progressWidth: computed('progress', function() {
        let progress = this.get('progress');
        if (typeof progress === 'number') {
            // clamp it and remove decimals
            progress = Math.round(Math.min(Math.max(progress, 0), 100));
            return htmlSafe(`width: ${progress}%`);
        }
        return null;
    }),

    click(event) {
        if (!event) {
            event = window.event;
        }
        event.preventDefault();
        this.action();
        return false;
    },

    // eslint-disable-next-line ember/no-observers
    toggleSpinner: observer('submitting', function () {
        let submitting = this.get('submitting');
        let timeout = this.get('showSpinnerTimeout');

        if (submitting) {
            this.set('showSpinner', true);

            if (this.get('timeout')) {
                this.set('showSpinnerTimeout', later(this, function () {
                    if (!this.get('submitting')) {
                        this.set('showSpinner', false);
                    }
                    this.set('showSpinnerTimeout', null);
                }, this.get('timeout')));
            }
        } else if (!submitting && timeout === null) {
            this.set('showSpinner', false);
        }
    }),

    // eslint-disable-next-line ember/no-observers
    setSize: observer('showSpinner', 'fixWidth', function () {
        if ((this.get('showSpinner') || this.get('fixWidth')) && this.get('autoWidth')) {
            this.element.style.width = `${this.element.offsetWidth}px`;
            this.element.style.height = `${this.element.offsetHeight}px`;
        } else {
            this.element.style.width = '';
            this.element.style.height = '';
        }
    }),

    willDestroy() {
        this._super(...arguments);
        cancel(this.get('showSpinnerTimeout'));
    }
});
