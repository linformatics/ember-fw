import Component from '@ember/component';
import {computed} from '@ember/object';
import ValidatedInputMixin from '@bennerinformatics/ember-fw/mixins/validated-input';
import layout from '@bennerinformatics/ember-fw/templates/components/fw-validated-textarea';

/**
 * This is the validated component that replaces `FwTextarea`, so as expected it renders a textarea, which will validate, show red if an error, and display error messages.
 * Because this is always a textarea component, it is much less complex than the `FwValidatedInputContainer` component, but because of how this is written, the ONLY options
 * you are able to use are the ones detailed in this documentation (other properties that are able to be used in `FwTextarea` because it extends other textareas will not be able
 * to be used in this one because it doesn't extend a textarea due to the other aspects defined in this component). Here is an example of basic usage:
 * ```handlebars
 * <FwValidatedTextarea @model={{model}} @valuePath="desc" @label="Description" @update={{action (mut model.desc)}} />
 * ```
 * @module Components
 * @extends [ValidatedInputMixin](ValidatedInputMixin)
 * @class FwValidatedTextarea
 */
export default Component.extend(ValidatedInputMixin, {
    layout,
    classNames: 'form-group',
    classNameBindings: 'state',

    inputId: computed('elementId', function() {
        return `${this.get('elementId')}-input`;
    }),
    /**
     * Sets the name property on the textarea-autoresize component.
     * @property name
     * @type {String}
     */
    name: '',
    /**
     * Placeholder is the placeholder of the text area. It displays inside the textarea whenever the text area is empty.
     * For more information [click here](https://www.w3schools.com/tags/att_placeholder.asp).
     * @property placeholder
     * @type {String}
     */
    placeholder: '',
    /**
     * Label is the words that are displayed as a label right before the textarea. Because of this property, there is no need to define a label before the component in your code.
     * @property label
     * @type {String}
     */
    label: '',
    /**
     * This is the text that will display in place of the error message if there is no error message to show (if there is an error message, that will trump this message)
     * @property helpText
     * @type {String}
     */
    helpText: '',
    /**
     * This is the message displayed in the `FwInfoIcon` directly after the label. (this info icon will be before whatever content is in the afterLabel property)
     * @property infoText
     * @type {String}
     */
    infoText: null,
    /**
     * This boolean will determine if the text area is usable. A disabled textarea will display the value, but be unable to be edited.
     * For more information [click here](https://www.w3schools.com/tags/att_disabled.asp)
     * @property disabled
     * @type {Boolean}
     */
    disabled: false,

    /**
     * If true, the cursor will begin in this textarea when it is rendered.
     * @property focus
     * @type {Boolean}
     * @default false
     */
    focus: false,

    /**
     * Number of lines in height the textarea is to start out. Use this or `min-height`, not both.
     * @property rows
     * @type {Number}
     * @default 2
     */
    rows: 2,
    /**
     * Goes with autoresize. This is the minimum height an empty textarea will be. This is also a css string for height. If autoresize is false, this is set to 56.8px.
     * @property min-height
     * @type {String}
     */
    /**
     * Goes with autoresize. This is the maximum height a FwTextarea will grow to. If it is null, the FwTextarea will grow indefinitely. If autoresize is false, it is set to 56.8 px.
     * @property max-height
     * @type {String}
     */
    /**
    * Autoresize allows the textarea to expand as more data is put into it. This is expanded with the `ember-cli-autoresize`.
    * @property autoresize
    * @type {Boolean}
    * @default true
    */
    autoresize: true,
    /**
     * If true, number of characters remaining will appear below the textarea, on the right.
     * @property showCount
     * @type {Boolean}
     * @default true
     */
    showCount: true,
    /**
     * Sets the limit that will be displayed. See also [maxCount](https://linformatics.bitbucket.io/docs/addons/client-api/ember-fw/classes/FwValidatedTextarea.html#property_maxCount).
     * @property maxlength
     * @type {Number}
     * @default 3000
     */
    maxlength: 3000,
    /**
     * Sets the limit of how many characters are allowed to be typed. Whichever of maxlength or maxCount is greater will determine the actual limit, but the count will display as negative and red if maxlength is surpassed.
     * @property maxCount
     * @type {Number}
     * @default 3000
     */
    maxCount: 3000,

    /**
     * This is an HTML safe property which will display whatever vanilla HTML you give it after the label and before the text area. (note: since it is vanilla HtML, it will not render
     * components properly)
     * @property afterLabel
     * @type {String}
     */
    didReceiveAttrs() {
        this._super(...arguments);
        if (!this.get('autoresize')) {
            this.set('max-height', '56.8px');
            this.set('min-height', '56.8px');
            this.set('rows', 2);
        }
    }
    /**
     * Key being validated by this field
     * @property valuePath
     * @type {String}
     * @inherited
     */

    /**
     * Action to call when the input changes
     * @property update
     * @type {Action}
     * @inherited
     */

    /**
     * Action to call when clicking out of the text field
     * @property focus-out
     * @type {Action}
     * @inherited
     */

    /**
     * Model containing the validations
     * @property model
     * @type {DS.Model}
     * @inherited
     */

    /**
     * If true, validation style classes will be skipped
     * @property disableStyle
     * @type {Boolean}
     * @default false
     * @inherited
     */

    /**
     * Current value of the object
     * @private
     * @property validation
     * @inherited
     */
    /**
     * Current value of the object
     * @private
     * @property value
     * @inherited
     */
});
