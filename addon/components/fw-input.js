import Component from '@ember/component';
import layout from '../templates/components/fw-input';
import {computed} from '@ember/object';

/**
 * Basically an extension of Ember's native `Input` component,
 * but applies some Bootstrap styling. See Ember `Input`
 * [documentation](https://guides.emberjs.com/v3.12.0/templates/input-helpers/)
 * for more information.
 *
 * If you want input-group-addons, you will need to pass them into the component like so.
 * This is to ensure that the character count doesn't mess with the addon. Note that you should not wrap it in the 'input-group' class, as that is done within the component:
 * ```handlebars
 * <FwInput @value={{myVariable}} @update={{action (mut myvariable)}} as |section|>
 *     {{#if section.prepend}}
 *         {{!-- insert prepend addon here --}}
 *     {{else if section.append}}
 *         {{!-- inset append addon here --}}
 *     {{/if}}
 * </FwInput>
 * ```
 *
 * @class FwInput
 * @extends Ember.TextField
 * @module Components
 */
export default Component.extend({
    layout,
    /**
     * This is the only added attribute to the Ember input. If true, it will focus on the input when the page loads.
     * @property focus
     * @type {Boolean}
     * @default false
     */
    focus: false,
    /**
      * After the elements have been inserted into the DOM, this function focuses upon this input (meaning that the cursor is automatically in it)
      * @method didInsertElement
      * @private
      */
    didInsertElement() {
        this._super(...arguments);

        if (this.get('focus')) {
            this.element.focus();
        }
    },
    /**
     * The variable that is the value of the input (the information displayed in it).
     * @property value
     * @type {String}
     * @default null
     * @inherited
     */
    /**
     * This is an optional attribute that changes the placeholder of the input (the grey text which appears if the textarea is empty).
     * @property placeholder
     * @type {String}
     * @default null
     * @inherited
     */
    /**
     * This attribute takes the action which is called whenever the value is changed. It is always used with the native handlebars action helper, and is often used with the native handlebars [mut](https://api.emberjs.com/ember/2.18/classes/Ember.Templates.helpers/methods?anchor=mut) helper (as seen in the example above).
     * @property update
     * @type {Action}
     * @inherited
     */
    /**
     * If true, number of characters remaining will appear below the input, on the right.
     * @property showCount
     * @type {Boolean}
     * @default true
     */
    showCount: true,
    /**
     * Sets the limit that will be displayed. See also [maxCount](https://linformatics.bitbucket.io/docs/addons/client-api/ember-fw/classes/FwInput.html#property_maxCount).
     * @property maxlength
     * @type {Number}
     * @default 200
     */
    maxlength: 200,
    /**
     * Sets the limit of how many characters are allowed to be typed. Whichever of maxlength or maxCount is greater will determine the actual limit, but the count will display as negative and red if maxlength is surpassed.
     * @property maxCount
     * @type {Boolean}
     * @default 200
     */
    maxCount: 200,

    characterLimit: computed('maxlength', 'maxCount', function() {
        let maxlength = this.get('maxlength');
        let maxCount = this.get('maxCount');
        // return whichever is greater
        return (maxlength > maxCount) ? maxlength : maxCount;
    }),

    charactersRemaining: computed('value', 'maxlength', function() {
        let value = this.get('value');
        return this.get('maxlength') - (value ? value.length : 0);
    }),

    countClass: computed('charactersRemaining', function() {
        if (this.get('charactersRemaining') < 0) {
            return 'characters-remaining-over pull-right';
        } else {
            return 'characters-remaining pull-right';
        }
    })
});
