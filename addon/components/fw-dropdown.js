import Component from '@ember/component';
import {computed} from '@ember/object';
import layout from '../templates/components/fw-dropdown';

/**
 * This component is used to generate a basic bootstrap dropdown.
 * The contents of the dropdown will be wrapped in the `fw-collection-ul` and `link-ul` classes
 * It works very well for making dropdowns especially for menu bars.
 *
 * Basic Usage:
 * ```handlebars
 * <FwDropdown @label='Dropdown'>
 *     <a href="http://example.com">Link 1</a>
 *     <a href="http://example.com">Link 2</a>
 * </FwDropdown>
 * ```
 *
 * @class FwDropdown
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    layout,
    classNameBindings: ['_sideClass'],

    /**
     * Computes the type of "dropdown" will be used (whether the dropdown is on the left, right, top, or bottom). Watches: `side`
     * @property _sideClass
     * @type {Computed Property}
     * @return {String} Dropdown Side class
     * @private
     */
    _sideClass: computed('side', function() {
        return `drop${this.get('side')}`;
    }).readOnly(),

    /**
     * Side for the dropdown, can be set to left, right, up, or down
     * @property side
     * @type {String}
     * @default 'down'
     */
    side: 'down',

    /**
     * If true, align the menu on the right. Note: if `side` is set to 'left' or 'right', this attribute is overridden doesn't do anything.
     * @property right
     * @type {Boolean}
     * @default false
     */
    right: false,

    /**
     * label for the dropdown toggle
     * @property label
     * @type {String}
     * @default null
     */
    label: null,

    /**
     * HTML Tag for the dropdown toggle. Often this will be set to `li` because other links in the menu bar are in an `ul`
     * @property toggleTag
     * @type {String}
     * @default 'button'
     */
    toggleTag: 'button',

    /**
     * Button style for the dropdown toggle without the 'btn-' prefix. Should be one of the bootstrap styles, commonly: 'default', 'primary',
     * 'danger'. See [bootstrap docs](https://getbootstrap.com/docs/4.0/components/buttons/) for more info. If this is null, 'btn-default' class is used.ed
     * @property toggleStyle
     * @type {String}
     * @default null
     */
    toggleStyle: null,

    /**
     * Font Awesome default icon to use for the toggle. Must have icon name and prefix
     * @property icon
     * @type {String}
     * @default null
     */
    icon: null,

    /**
     * Font Awesome icon to use for the toggle when open. Must have icon name and prefix
     * @property iconOpen
     * @type {String}
     * @default null
     */
    iconOpen: null,

    /**
     * Font Awesome icon to use for the button when closed. Must have icon name and prefix
     * @property iconClose
     * @type {String}
     * @default null
     */
    iconClose: null,

    /**
     * If currently viewing the route identified by this attribute, the active CSS class is added to the link (usually making it bold).
     * @property active
     * @type {String}
     * @default null
     */
    active: null

    /**
     * The tagName property will often be set in the `FwDropdown`, so it is worth mentioning, though it is inhereted by Components.
     * For documentation on this functionaity, [click here](https://guides.emberjs.com/v2.18.0/components/customizing-a-components-element/#toc_customizing-the-element)
     * @property tagName
     * @type {String}
     * @inherited
     */
});
