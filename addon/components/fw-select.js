import Component from '@ember/component';
import {reads} from '@ember/object/computed';
import layout from '@bennerinformatics/ember-fw/templates/components/fw-select';

/**
 * Ember-style select component. Mimics the behavior of a native `<select>` element
 * as closely as possible.
 *
 * Usage:
 *
 * ```handlebars
 * <FwSelect @content={{options}} @selection={{selection}} @action={{action "onChange"}} />
 * ```
 *
 * The above will render a select element with all of the options specified in `content`
 * and the option specified in `selection` specified, if any.
 *
 * @class FwSelect
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    layout,
    tagName: 'select',
    classNames: 'form-control',

    /**
     * Action called on input update
     *
     * @property action
     * @type {Action}
     */
    action: () => {},

    /**
     * Array of options to render. Importantly, these options should be objects, otherwise dropdown will not function as expeced.
     *
     * @property content
     * @type {Array}
     * @default []
     */
    content: [],

    /**
     * If you want there to be a prompt in the select (e.g. "Select an option..."),
     * specify it here and it will be rendered as a disabled option in the first position.
     *
     * @property prompt
     * @type {String}
     */
    prompt: '',

    /**
     * If there is an already selected object, specify it here and the select component
     * will try to show it as the currently selected option.
     *
     * @property selection
     * @type {Object}
     */
    selection: null,

    /**
     * Optional key of the element to display as the "value" of the option
     * element. Doesn't do anything in terms of functionality for this component.
     *
     * @property optionValuePath
     * @type {String}
     * @default 'id'
     */
    optionValuePath: 'id',

    /**
     * Specifies the property of the element to be displayed as the text in the
     * <option> element.
     *
     * @property optionLabelPath
     * @type {String}
     * @default 'name'
     */
    optionLabelPath: 'name',

    /**
     * Whether or not the prompt can be selected as a valid option. This is useful
     * if you want to allow the selection to be null.
     *
     * @property promptEnabled
     * @type {Boolean}
     * @default false
     */
    promptEnabled: false,

    change() {
        this.send('change');
    },

    _selection: reads('selection'),

    actions: {
        change() {
            let selectIndex = this.element.selectedIndex;
            let content = this.get('content');
            let hasPrompt = !!this.get('prompt');
            let contentIndex = hasPrompt ? selectIndex - 1 : selectIndex;
            let selection;

            if (this.get('promptEnabled') && contentIndex === -1) {
                selection = '';
            } else {
                selection = content.objectAt(contentIndex);
            }

            this.set('_selection', selection);

            this.action(selection);
        }
    }
});
