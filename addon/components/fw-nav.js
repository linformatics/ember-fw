import Component from '@ember/component';

/**
 * Basic navigation component
 *
 * Usage:
 *
 * ```handlebars
 * <FwNav>
 *     <ul>
 *         <li>
 *             <LinkTo @route="index">Index</LinkTo>
 *     </ul>
 * </FwNav>
 * ```
 *
 * It's designed so that you can show/hide the links any way you want. For example,
 * if you wanted to only show a link based on if a user was logged in, you could wrap
 * it in an `auth-block` component:
 *
 * ```handlebars
 * <FwNav>
 *     <ul>
 *         <AuthBlock @tagName="li">
 *             <LinkTo @route="index">Index</LinkTo>
 *         </AuthBlock>
 *     </ul>
 * </FwNav>
 * ```
 *
 * Result: The `index` link only shows when the user is logged in.
 *
 * Note: this is extended in ember-fw-gc by FwGcNav, so you might want to check out its documentation [here](../../ember-fw-gc/classes/FwGcNav.html).
 * @class FwNav
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    tagName: 'nav',
    classNames: 'fw-main-nav'
});
