import Component from '@ember/component';
import {assert} from '@ember/debug';
import {computed} from '@ember/object';
import {inject} from '@ember/service';
import layout from '@bennerinformatics/ember-fw/templates/components/fw-header';

/**
 * Page Header Component. Used to render important parts of the header. In Ember FW, the only
 * thing which is rendered is the logo and name (or shortname for mobile) on the left hand
 * side of the header, which links to the home page. However, this component is extended by
 * ember-fw-gc, so you can check out its documentation [here](../../ember-fw-gc/FwHeaderGc.html) as well
 * to see the full description of what is used here.
 *
 * Basic Usage:
 * ```handlebars
 * <FwHeader />
 * ```
 *
 * @class FwHeader
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    layout,
    tagName: 'header',
    classNames: ['fw-header'],

    // Services
    config: inject(),
    /**
     * Logo is the app logo that will appear in the left corner of the screen. This is defined by the config property (config.logo).
     * @private
     * @property logo
     * @type {Computed}
     */
    logo: computed(function () {
        assert('Logo must be a property set on the config service', this.get('config.logo'));
        return this.get('config.logo') || 'http://placehold.it/45x45/';
    })
});
