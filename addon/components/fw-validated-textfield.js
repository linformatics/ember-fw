import Component from '@ember/component';
import {computed} from '@ember/object';
import ValidatedInputMixin from '@bennerinformatics/ember-fw/mixins/validated-input';
import layout from '@bennerinformatics/ember-fw/templates/components/fw-validated-textfield';

/**
 * This is the simplest of all of the validated compoenents, as it replaces `FwInput`. Essentially, this component is
 * designed to be used for the simplest data entry - just simple strings. Included with this textfield, however, is it will validate
 * and display the error messages automatically on invalid data. Also, because of the nature of this component, it does not extend
 * any other input component, so the ONLY options avaiable to it are those listed in this documentation (ie even if the option is valid on `FwInput`, if its not listed, it isn't available). Here is the basic usage:
 * ```hbs
 * <FwValidatedTextField @model={{model}} @valuePath="name" @label="Name" @update={{action (mut model.name)}} />
 * ```
 * @module Components
 * @extends [ValidatedInputMixin](ValidatedInputMixin)
 * @class FwValidatedTextfield
 */
export default Component.extend(ValidatedInputMixin, {
    layout,
    classNames: 'form-group',
    classNameBindings: 'state',

    inputId: computed('elementId', function() {
        return `${this.get('elementId')}-textarea`;
    }),
    /**
     * This is the many different types of inputs that there are. For a list of valid input types, [click here](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#input_types).
     * While it is hypothetically feasible to use any of these types with this component, this component is usually used only for textfields, so much further testing would be necessary before
     * using it for any other input type.
     * @property type
     * @type {String}
     * @default 'text'
     */
    type: 'text',
    /**
     * Basic input name attribute. [Click here for more details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#name)
     * @property name
     * @type {String}
     */
    name: '',
    /**
     * Placeholder is the placeholder of the input. It displays inside the textfield whenever the text area is empty.
     * For more information [click here](https://www.w3schools.com/tags/att_placeholder.asp).
     * @property placeholder
     * @type {String}
     */
    placeholder: null,
    /**
     * Label is the words that are displayed as a label right before the textfield. Because of this property, there is no need to define a label before the component in your code.
     * @property label
     * @type {String}
     */
    label: '',
    /**
     * This is the text that will display in place of the error message if there is no error message to show (if there is an error message, that will trump this message)
     * @property helpText
     * @type {String}
     */
    helpText: '',
    /**
     * This boolean will determine if the text area is usable. A disabled textarea will display the value, but be unable to be edited.
     * For more information [click here](https://www.w3schools.com/tags/att_disabled.asp)
     * @property disabled
     * @type {Boolean}
     */
    disabled: false,
    /**
     * This is the message displayed in the `FwInfoIcon` directly after the label. (this info icon will be before whatever content is in the afterLabel property)
     * @property infoText
     * @type {String}
     */
    infoText: null,

    // other options
    /**
     * Basic Input readonly attribute [Click here for more details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#readonly)
     * @property readonly
     * @type {Boolean}
     */
    readonly: null,
    /**
     * Basic Input size attribute. [Click here for more details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#size)
     * @property size
     * @type {String}
     */
    size: null,
    /**
     * If true, number of characters remaining will appear below the textarea, on the right.
     * @property showCount
     * @type {Boolean}
     * @default true
     */
    showCount: true,
    /**
     * Sets the limit that will be displayed. See also [maxCount](https://linformatics.bitbucket.io/docs/addons/client-api/ember-fw/classes/FWValidatedTextarea.html#property_maxCount).
     * @property maxlength
     * @type {Number}
     * @default 3000
     */
    maxlength: 200,
    /**
     * Sets the limit of how many characters are allowed to be typed. Whichever of maxlength or maxCount is greater will determine the actual limit, but the count will display as negative and red if maxlength is surpassed.
     * @property maxCount
     * @type {Number}
     * @default 3000
     */
    maxCount: 200,

    /**
     * Basic Input autocomplete attribute. [Click here for more details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#autocomplete)
     * @property autocomplete
     * @type {String}
     */
    autocomplete: null,
    /**
     * Basic Input autofocus attribute. [Click here for more details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#autofocus)
     * @property autofocus
     * @type {Boolean}
     */
    autofocus: null,
    /**
     * Basic Input formnovalidate attribute. [Click here for more details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#formnovalidate)
     * @property novalidate
     * @type {Boolean}
     */
    novalidate: null,
    /**
     * Basic input min value. [Click here for details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#min)
     * @property min
     * @type {Number}
     */
    min: null,
    /**
     * Basic input max value [Click here for details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#max)
     * @property max
     * @type {String}
     */
    max: null,
    /**
     * Basic Input step attribute. [Click here for more details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#step)
     * @property step
     * @type {Number}
     */
    step: null,
    /**
     * Basic Input pattern attribute. [Click here for more details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#pattern)
     * @property pattern
     * @type {String}
     */
    pattern: null,
    /**
     * Basic Input focus attribute. [Click here for more details](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#focus)
     * @property focus
     * @type {String}
     */
    focus: false
    /**
     * Key being validated by this field
     * @property valuePath
     * @type {String}
     * @inherited
     */

    /**
     * Action to call when the input changes
     * @property update
     * @type {Action}
     * @inherited
     */

    /**
     * Action to call when clicking out of the text field
     * @property focus-out
     * @type {Action}
     * @inherited
     */

    /**
     * Model containing the validations
     * @property model
     * @type {DS.Model}
     * @inherited
     */

    /**
     * If true, validation style classes will be skipped
     * @property disableStyle
     * @type {Boolean}
     * @default false
     * @inherited
     */

    /**
     * Current value of the object
     * @private
     * @property validation
     * @inherited
     */
    /**
     * Current value of the object
     * @private
     * @property value
     * @inherited
     */
});
