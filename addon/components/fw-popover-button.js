import Component from '@ember/component';
import PopoverMixin from '@bennerinformatics/ember-fw/mixins/popover';

/**
 * Button that comes with the popover mixin. Essentially only difference between this and [FwPopoverIcon](FwPopoverIcon.html) is that the tagName is `button` rather than `i`.
 *
 * @class FwPopoverButton
 * @extends Ember.Component
 * @uses [PopoverMixin](PopoverMixin.html)
 * @module Components
 */
export default Component.extend(PopoverMixin, {
    tagName: 'button',

    /**
     * Either closure action to call when the button is clicked
     *
     * @property action
     */
    action: () => {},

    /**
     * Placement of the popover (top, bottom, left, right, or auto)
     * auto is a special placement which will appear top or bottom based on location on the screen
     * @property placement
     */

    /**
     * Title of the popover
     * @property title
     */

    /**
     * Content of the popover
     * @property content
     */

    /**
     * Trigger of the popover (hover, click, focus, or pinnable)
     * pinnable is special behavior which supports both click and hover
     * @property popoverTrigger
     */

    /**
     * Container for the popover, set to 'body' to prevent parent styling from modifying the popover
     * @property container
     */

    /**
     * Captures the click of a button and calls an optional "action" property
     *
     * @method click
     */
    click() {
        this.action();
    }
});
