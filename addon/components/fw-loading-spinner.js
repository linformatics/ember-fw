import Component from '@ember/component';
import {task, timeout} from 'ember-concurrency';

import layout from '@bennerinformatics/ember-fw/templates/components/fw-loading-spinner';
/**
 * This component is used to show our primary loading spinner that we use when loading pages. To use it you simply call the function without changing any attribures:
 *
 * ```handlebars
 * <FwLoadingSpinner />
 * ```
 *
 * @class FwLoadingSpinner
 * @module Components
 */
export default Component.extend({
    layout,

    classNames: 'fw-loading',

    /**
     * An internal property that will be set to true after a time out. It begins by hidden so the spinner flash.
     * @private
     * @property showSpinner
     * @type {Boolean}
     * @default false
     */
    showSpinner: false,
    /**
     * Milliseconds until spinner is shown. This prevents the flash of the spinner.
     * @property slowLoadTimeout
     * @private
     * @type {Number}
     * @default 200
     */
    slowLoadTimeout: 200,

    /**
     * Task function which is performed. Waits the slowLoadTimeout amount of time before displaying the spinner.
     * @method startSpinnerTimeout
     * @private
     * @type {Task}
     */
    startSpinnerTimeout: task(function* () {
        yield timeout(this.get('slowLoadTimeout'));
        this.set('showSpinner', true);
    }),
    /**
     * After all elements have been inserted, it performs the startSpinnerTimeout task.
     * @method didInsertElement
     * @private
     */
    didInsertElement() {
        this.get('startSpinnerTimeout').perform();
    }
});
