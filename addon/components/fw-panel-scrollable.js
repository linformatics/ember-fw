import Component from '@ember/component';
import {computed} from '@ember/object';
/**
 * This component is used to generate a scrollable section of a panel
 * It will take a couple of parameters to customize it, but it will display the scrollbar and format it
 *
 * Basic Usage:
 *
 * ```handlebars
 * <FwPanelScrollable @maxHeight='200px' @hideOnMouseLeave={{false}} @scrollbarThickness='thin'>
 *      {{!...stuff to be in the panel}}
 * </FwPanelScrollable>
 * ```
 *
 * Advanced Usage Notes:
 *
 * This scrollable panel works in a difficult way with power-select, so it needs special attention in order to work properly.
 * If not configured properly, power select drop down menu will be clipped and hidden underneath the scrollable panel. This has to do with the `renderInPlace=true` on the power select, which is there to make it work with modals. Here is the fix:
 * * **Scenario 1**. If you are NOT in a modal, simply remove `@renderInPlace={{true}}` from the power select, and it will work again.
 * * **Scenario 2**. If you ARE in a modal, you have a few steps.
 *      - Remove `@renderInPlace={{true}}` from the power select
 *      - The line above the FwPanelScrollable, put the following code:
 *      `<div id="uniqueDivId"></div>`
 *      - Then in the PowerSelect component write add the attribute, `@destination="uniqueDivId"`.
 *      - Finally, in your FwFullscreenModal component, add the attribute: `@scrollTopReturn=true`.
 *
 * With these four steps, the PowerSelect should be working in modals perfectly as well.
 *
 * @class FwPanelScrollable
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    // base class names for the div element
    classNames: ['scrollable', 'firefox-scroll'],

    // if user wants to hide on mouse leave, add classname to do that
    classNameBindings: ['hideOnMouseLeave:hideScroll:noHide', 'thickClass'],

    // add the style attribute to change max height
    attributeBindings: ['style'],

    /**
     * Style is a computed property to be bounded as an attribute to the element of the component. We use it to set maxHeight. Watches: maxHeight
     * @property style
     * @type {Computed}
     * @private
     */
    style: computed('maxHeight', function() {
        return `max-height: ${this.get('maxHeight')};`;
    }),

    /**
     * This takes a CSS string for height (meaning a number with the units px, em, or even vh would work). It gives the maximum height allowed before the scrollbar appears.
     * **Note**: on a regular modal (with the normal header and footer and just a scrollable panel), setting the section to have a scrollable panel of height 67vh (viewport height) is the perfect size fit for any screen. If you add things outside the scrollable  panel, playing with this might be helpful.
     * @property maxHeight
     * @type {String}
     * @default '200px'
     */
    maxHeight: '200px',

    /**
     * If this is true, the scrollbar will only show whenever the mouse is hovering over the panel and when it is not, it will be hidden. Otherwise, the scrollbar will appear all the time.
     * @property hideOnMouseLeave
     * @type {Boolean}
     * @default false
     */
    hideOnMouseLeave: false,

    /**
     * This sets the thickness that the scrollbar will appear. Options: thin, medium, and thick. Note: on Firefox, medium and thick appear the same, only Chrome displays a difference between these two.
     * @property scrollbarThickness
     * @type {String}
     * @default 'thin'
     */
    scrollbarThickness: 'thin',

    /**
     * If true, this sets the positioning of the scrollbar to start at the bottom of the scrollable content. Otherwise, it starts the scrollbar at the top as normal.
     * @property scrollBottom
     * @type {Boolean}
     * @default false
     */
    scrollBottom: false,

    /**
     * After the elements have been inserted into the DOM, this function sets the scrollbar to the bottom or top, depending on scrollBottom property.
     * @method didInsertElement
     * @private
     */
    didInsertElement() {
        this._super(...arguments);
        // set positioning of scroll bar if scrollBottom is true, scrollbar starts at bottom of the
        // scroll. By default it starts at top.
        this.element.scrollTop = this.get('scrollBottom') ? this.element.scrollHeight : 0;
    },
    /**
     * This is a computed class which takes the scrollbarThickness attribute and applies the proper class to the element. This way if scrollbarThickness is set to an invalid class, a valid class is selected instead.
     * @property thickClass
     * @private
     * @type {Computed}
     */
    thickClass: computed('scrollbarThickness', function() {
        let thickness = this.get('scrollbarThickness');
        if (thickness == 'large' || thickness == 'thick') {
            return 'thick';
        }
        if (thickness == 'medium' || thickness == 'middle') {
            return 'medium';
        }
        return 'thin';
    })
});
