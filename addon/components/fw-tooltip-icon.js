import Component from '@ember/component';
import TooltipMixin from '@bennerinformatics/ember-fw/mixins/tooltip';

/**
 * Icon that comes with the popover mixin
 *
 * @class FwTooltipIcon
 * @extends Ember.Component
 * @uses [TooltipMixin](Tooltip.html)
 * @module Components
 */
export default Component.extend(TooltipMixin, {
    /**
     * Font-Awesome icon name
     * @property icon
     * @type {String}
     * @default 'fa-solid fa-info'
     */
    icon: 'fa-solid fa-info',

    tagName: 'i',
    classNameBindings: ['icon']

    /**
     * Placement of the popover (top, bottom, left, or right)
     * @property placement
     * @type {String}
     */

    /**
     * text of the popover
     * @property text
     * @type {String}
     */
});
