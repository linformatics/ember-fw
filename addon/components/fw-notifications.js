import Component from '@ember/component';
import {inject as service} from '@ember/service';
import {filter} from '@ember/object/computed';
import layout from '@bennerinformatics/ember-fw/templates/components/fw-notifications';

/**
 * Used for notifications. Can also be placed anywhere in the applcation, given a provided "outlet" property, and notifications with that outlet will appear inside it. Without this properly setup, notifications will not display properly.
 *
 * Example:
 * ```handlebars
 * <FwNotifications @outlet="global">
 * ```
 *
 * In the example, notifications with the "global" outlet specified will be rendered
 * inside this component using the Notification component. See the [notifications service](NotificationsService.html)
 * documentation for more information.
 *
 * @class FwNotifications
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    layout,
    tagName: 'aside',

    /**
     * The outlet to look for notifications in.
     *
     * @property outlet
     * @type {String}
     * @default 'default'
     */
    outlet: 'default',

    classNames: 'alert-container',
    classNameBindings: 'outlet',

    notifications: service(),

    /**
     * NotificationsToShow is actually the filtered list of notifications that are currently appearing in this notifications outlet. It filters all active notifications based on the outlet.
     * @properrty notificationsToShow
     * @type {Computed}
     * @private
     */
    notificationsToShow: filter('notifications.content', function (item) {
        return item.outlet === this.get('outlet');
    })
});
