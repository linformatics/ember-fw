import Component from '@ember/component';
import TooltipMixin from '@bennerinformatics/ember-fw/mixins/tooltip';

/**
 * Button that comes with the tooltip mixin
 *
 * @class FwTooltipButton
 * @extends Ember.Component
 * @uses [TooltipMixin](Tooltip.html)
 * @module Components
 */
export default Component.extend(TooltipMixin, {
    tagName: 'button',

    /**
     * Either closure action to call when the button is clicked.
     *
     * @property action
     */
    action: () => {},

    /**
     * Placement of the popover (top, bottom, left, or right)
     * @property placement
     */

    /**
     * text of the popover
     * @property text
     */

    /**
     * Captures the click of a button and calls an optional "action" property
     *
     * @method click
     */
    click() {
        this.action();
    }
});
