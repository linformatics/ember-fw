import Component from '@ember/component';
import PopoverMixin from '@bennerinformatics/ember-fw/mixins/popover';

/**
 * Icon that comes with the popover mixin. Works the same as [FwPopoverButton](FwPopoverButton.html), except it uses `i` as tag instead of `button`.
 *
 * Note: In most cases, you should use [FwInfoIcon](FwInfoIcon.html), which is more streamlined to use.
 *
 *
 * @class FwPopoverIcon
 * @extends Ember.Component
 * @uses [PopoverMixin](PopoverMixin.html)
 * @module Components
 */
export default Component.extend(PopoverMixin, {
    /**
     * Font-Awesome icon name and class
     * @property icon
     * @type {String}
     * @default 'fa-solid fa-info'
     */
    icon: 'fa-solid fa-info',

    tagName: 'i',
    classNameBindings: ['icon']

    /**
     * Placement of the popover (top, bottom, left, right, or auto)
     * auto is a special placement which will appear top or bottom based on location on the screen
     * @property placement
     */

    /**
     * Title of the popover
     * @property title
     */

    /**
     * Content of the popover
     * @property content
     */

    /**
     * Container for the popover, set to 'body' to prevent parent styling from modifying the popover
     * @property container
     */

    /**
     * Trigger of the popover (hover, click, focus, or pinnable)
     * pinnable is special behavior which supports both click and hover
     * @property popoverTrigger
     */
});
