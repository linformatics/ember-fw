import Component from '@ember/component';
import ValidatedInputMixin from '@bennerinformatics/ember-fw/mixins/validated-input';
import layout from '@bennerinformatics/ember-fw/templates/components/fw-validated-input-container';

/**
 * This is the most versatile of the three validated components. It is a block component, so you wrap any other component with it to validate that component (commonly used with
 * power-select or bs-datetimepicker). The attributes able to be passed to the component itself are listed in the properties, though the ones that are absolutely necessary to be filled out
 * are `valuePath`, `model`, and `update`.
 *
 * Now, there is also content passed to the yield block, which is important in properly using this component. If you need to know more about how this works in Ember,
 * [checkout this documentation](https://guides.emberjs.com/v3.12.0/components/wrapping-content-in-a-component/). The three parameters shared
 * in this way are the following:
 * * `value` - The first parameter contains the value of the input container. This is the computed result of the valuePath and model. In the example below, you will notice that the selected attribute
 * of the power-select within the wrapped content should use value NOT computing it themselves (for example NOT `selected={{model.location}}`) as a lot of apps already do.
 * * `message` - The second parameter contains the error message if the input validation is invalid. If it has validated correctly, this will be an empty string. Notice in the example
 * that it is vital that you place the code at the bottom of the block helper to display the message if it is defined.
 * * `actions` - The third parameter contains two actions: update and onblur. These properties should be used rather than using the action helper within the block (example NOT
 * `onchange={{action 'updateLocation'}}``). Anything you wish to do action wise should be passed into the fw-validated-input-container, not passed in directly to the components
 * within its block. (see example below for clarification).
 *
 * Note: nothing is "automatically" defined for you, such as the message display or label, as with the other validated input components (hence why it is a "container"), so you will need
 * to define both the spot where the error message will be displayed and the label for the component. Here is a proper example of a validated power-select of location:
 * ```hbs
 * <FwValidatedInputContainer @valuePath='location' @model={{model}} @update={{action 'updateLocation'}} as |value message actions|>
 *      <label>Location</label>
 *      <PowerSelect @options={{sortedLocations}} @selected={{value}} @placeholder=" " @noMatchesMessage="No results" @onchange={{actions.update}} @onblur={{actions.onblur}} @renderInPlace={{true}} @searchField="title" as |location|>
 *          {{location.title}}
 *      </PowerSelect>
 *      {{#if message}}
 *          <span class="help-block">{{message}}</span>
 *      {{/if}}
 * </FwValidatedInputContainer>
 * ```
 * @module Components
 * @extends [ValidatedInputMixin](ValidatedInputMixin)
 * @class FwValidatedInputContainer
 */
export default Component.extend(ValidatedInputMixin, {
    layout,

    classNames: 'form-group',
    classNameBindings: 'state'

    /**
     * Key being validated by this field
     * @property valuePath
     * @type {String}
     * @inherited
     */

    /**
     * Action to call when the input changes
     * @property update
     * @type {Action}
     */

    /**
     * Action to call when clicking out of the text field
     * @property focus-out
     * @type {Action}
     */

    /**
     * Model containing the validations
     * @property model
     * @type {DS.Model}
     */

    /**
     * If true, validation style classes will be skipped
     * @property disableStyle
     * @type {Boolean}
     * @default false
     */

    /**
     * Current value of the object
     * @private
     * @property validation
     */
    /**
     * Current value of the object
     * @private
     * @property value
     */
});
