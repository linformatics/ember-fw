import Component from '@ember/component';
import layout from '../templates/components/fw-textarea';
import {computed} from '@ember/object';
/**
 * An extension of Ember's `TextareaAutosize` component from ember-cli-textarea-autosize, but
 * applies Bootstrap styling. Basic elements needed to properly use are listed as properties here. Basic usage example:
 * ```handlebars
 * <FwTextarea @value={{myVariable}} @update={{action (mut myVariable)}} />
 * ```
 *
 * If you want input-group-addons, you will need to pass them into the component like so.
 * This is to ensure that the character count doesn't mess with the addon. Note that you should not wrap it in the 'input-group' class, as that is done within the component:
 * ```handlebars
 * <FwTextarea @value={{myVariable}} @update={{action (mut myvariable)}} as |section|>
 *     {{#if section.prepend}}
 *         {{!-- insert prepend addon here --}}
 *     {{else if section.append}}
 *         {{!-- inset append addon here --}}
 *     {{/if}}
 * </FwTextarea>
 * ```
 *
 * See Ember's `textarea`, [documentation here](https://guides.emberjs.com/v2.5.0/templates/input-helpers/) and ember-cli-textarea-autosize,
 * [documentation here](https://github.com/adopted-ember-addons/ember-autoresize#ember-autoresize--).
 * These two documentations should help you know how to do things.
 * for more information.
 *
 * @class FwTextarea
 * @extends Ember.Textarea
 * @module Components
 */
export default Component.extend({
    layout,
    /**
     * If true, the cursor will begin in this textarea when it is rendered.
     * @property focus
     * @type {Boolean}
     * @default false
     */
    focus: false,
    /**
     * The variable that is the value of the textarea (the information displayed in it).
     * @property value
     * @type {String}
     * @default null
     * @inherited
     */
    /**
     * This is an optional attribute that changes the placeholder of the textarea (the grey text which appears if the textarea is empty).
     * @property placeholder
     * @type {String}
     * @default null
     * @inherited
     */
    /**
     * This attribute takes the action which is called whenever the value is changed. It is always used with the native handlebars action helper, and is often used with the native handlebars [mut](https://api.emberjs.com/ember/2.18/classes/Ember.Templates.helpers/methods?anchor=mut) helper (as seen in the example above).
     * @property update
     * @type {Action}
     * @inherited
     */
    /**
     * Number of lines in height the textarea is to start out. Use this or `min-height`, not both.
     * @property rows
     * @type {Number}
     * @default 2
     * @inherited
     */
    /**
     * Goes with autoresize. This is the minimum height an empty textarea will be. This is also a css string for height. If autoresize is false, this is set to 56.8px.
     * @property min-height
     * @type {String}
     * @inherited
     */
    /**
     * Goes with autoresize. This is the maximum height a fw-textarea will grow to. If it is null, the fw-textarea will grow indefinitely. If autoresize is false, it is set to 56.8 px.
     * @property max-height
     * @type {String}
     * @inherited
     */
    /**
    * Autoresize allows the textarea to expand as more data is put into it. This is expanded with the ember-cli-autoresize.
    * @property autoresize
    * @type {Boolean}
    * @default true
    * @inherited
    */
    autoresize: true,
    /**
     * If true, number of characters remaining will appear below the textarea, on the right.
     * @property showCount
     * @type {Boolean}
     * @default true
     */
    showCount: true,
    /**
     * Sets the limit that will be displayed. See also [maxCount](https://linformatics.bitbucket.io/docs/addons/client-api/ember-fw/classes/FwTextarea.html#property_maxCount).
     * @property maxlength
     * @type {Number}
     * @default 3000
     */
    maxlength: 3000,
    /**
     * Sets the limit of how many characters are allowed to be typed. Whichever of maxlength or maxCount is greater will determine the actual limit, but the count will display as negative and red if maxlength is surpassed.
     * @property maxCount
     * @type {Number}
     * @default 3000
     */
    maxCount: 3000,

    characterLimit: computed('maxlength', 'maxCount', function() {
        let maxlength = this.get('maxlength');
        let maxCount = this.get('maxCount');
        // return whichever is greater
        return (maxlength > maxCount) ? maxlength : maxCount;
    }),

    charactersRemaining: computed('value', 'maxlength', function() {
        let value = this.get('value');
        return this.get('maxlength') - (value ? value.length : 0);
    }),

    countClass: computed('charactersRemaining', function() {
        if (this.get('charactersRemaining') < 0) {
            return 'characters-remaining-over pull-right';
        } else {
            return 'characters-remaining pull-right';
        }
    }),

    /**
     * After attributes are received, if autoresize is false, this sets the proper defaults.
     * @method didReceiveAttrs
     * @private
     */
    didReceiveAttrs() {
        this._super(...arguments);
        // set values to default if not autoresizing
        if (!this.get('autoresize')) {
            this.set('max-height', '56.8px');
            this.set('min-height', '56.8px');
            this.set('rows', 2);
        }
    },

    /**
     * After the element has been inserted into the DOM, this sets the focus to the textarea properly if focus is set to true.
     * @method didInsertElement
     * @private
     */
    didInsertElement() {
        this._super(...arguments);

        if (this.get('focus')) {
            this.element.focus();
        }
    }
});
