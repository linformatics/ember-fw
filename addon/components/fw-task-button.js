/* eslint-disable ember/no-side-effects */
import SpinButton from './fw-spin-button';
import {computed} from '@ember/object';
import {alias} from '@ember/object/computed';

import layout from '../templates/components/fw-task-button';
import {task, timeout} from 'ember-concurrency';

/**
 * This component is built to function just like a [FwPromiseButton](FwPromiseButton.html), except instead of working with promises it works with tasks.
 * For more information on ember tasks, [click here](https://riptutorial.com/ember-js/example/3398/ember-concurrency-task).
 *
 * ```handlebars
 * <FwTaskButton @buttonIcon="fa-regular fa-floppy-disk" @buttonText="Save" @class="btn btn-primary btn-sm" @task={{save}} />
 * ```
 * @class FwTaskButton
 * @module Components
 */
export default SpinButton.extend({
    layout,
    classNameBindings: 'buttonState',

    /**
     * Action to call when clicking the button
     * @type {Action}
     */
    action: () => {},

    /**
     * Task function to call when the button is clicked. This is required for button to function properly.
     * @property task
     * @type {Task}
     * @default null
     */
    task: null,

    /**
     * The time that the state of the button should be shown before returning to
     * default. Time must be specified in milliseconds.
     *
     * @property stateTimeout
     * @type {Number}
     * @default 3000
     */
    stateTimeout: 3000,

    // doesn't appear to be used anywhere in this component, so not actually doing documentation on it.
    timeout: false,
    /**
     * If true, this will prevent the task from being performed when the button is clicked. Otherwise it will work as normal.
     * @property disableClick
     * @type {Boolean}
     * @default false
     */
    disableClick: false,

    /**
     * This is a private boolean that logs whether the task has been completed or not.
     * @private
     * @property _taskHasRan
     * @type {Boolean}
     * @default false
     */
    _taskHasRan: false,

    submitting: alias('task.isRunning'),
    fixWidth: alias('_taskHasRan'),

    /**
     * Task that resets the button back to normal after the task has been run.
     * @method reset
     * @private
     */
    reset: task(function* (originalIcon) {
        yield timeout(this.get('stateTimeout'));
        this.set('_taskHasRan', false);
        this.set('buttonIcon', originalIcon);
    }),
    /**
     * If you are rendering the promise button as an inline component and not
     * in block form, set this to whatever text you want the button to display.
     * If you are rendering in block form, this does not apply.
     *
     * @property buttonText
     * @type {String}
     * @default ''
     */
    /**
     * If you want a button-icon to be displayed, put any of the font-awesome icons
     * as a string here. Must use full icon name, including the class
     *
     * @property buttonIcon
     * @type {String}
     * @default ''
     */
    /**
      * This returns the buttonStyle based on the state that the button is in.
      * If success, it is a success class; if error, danger class; and otherwise
      * it is the normal button class. Watches: submitting, _taskHasRan.
      * @property buttonState
      * @type {Computed}
      * @private
      */
    buttonState: computed('submitting', '_taskHasRan', function () {
        let {submitting, _taskHasRan} = this.getProperties('submitting', '_taskHasRan');

        if (submitting || (!submitting && !_taskHasRan)) {
            this.set('_taskHasRan', submitting);
            return '';
        }

        // reset state in `stateTimeout` milliseconds
        this.get('reset').perform(this.get('buttonIcon'));

        if (this.get('task.last.error')) {
            this.set('buttonIcon', 'fa-solid fa-xmark');
            return 'btn-danger';
        }

        this.set('buttonIcon', 'fa-solid fa-check');
        return 'btn-success';
    }),

    click(event) {
        if (!event) {
            event = window.event;
        }
        event.preventDefault();
        this.action();

        // cancel any state reset tasks
        this.get('reset').cancelAll();

        if (!this.get('disableClick')) {
            return this.get('task').perform().catch(() => {});
        }
    }
});
