import PowerSelect from 'ember-power-select/components/power-select';

export default PowerSelect.extend({
    // redirect tab to enter instead of making it close the select
    _handleKeyTab(e) {
        return this._handleKeyEnter(e);
    }
});
