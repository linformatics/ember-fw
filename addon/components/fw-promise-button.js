import {isNone} from '@ember/utils';
import Component from '@ember/component';
import {computed} from '@ember/object';
import {cancel, later} from '@ember/runloop';

import layout from '@bennerinformatics/ember-fw/templates/components/fw-promise-button';
import RSVP from 'rsvp';

/**
 * Promise-enabled buttons. PromiseButton is a component that will take a promise,
 * render as a button, and when clicked, will trigger the promise and then show its state
 * once the promise has either resolved or rejected. To read more on promises go
 * [here](https://guides.emberjs.com/v3.12.0/routing/asynchronous-routing/#toc_a-word-on-promises).
 *
 * Usage:
 * ```handlebars
 * <FwPromiseButton @class="btn btn-sm" @promise={{action 'callServer'}}>
 *     Click Me!
 * </FwPromiseButton>
 * ```
 *
 * The `promise` variable is a closure action that must return a promise. If not,
 * unexpected behavior may occur. The promise option is the only one that _needs_
 * to be specified.
 *
 * _When should you use the promise-button as opposed to HTML generic button?_
 * General rule of thumb, if the action needs to call a network request, then it
 * should return a promise and use a promise button. If it doesn’t call a network
 * request, then it should use the plain [button](https://www.w3schools.com/tags/tag_button.asp)
 * HTML tag.
 *
 * @class FwPromiseButton
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    layout,
    tagName: '',

    /**
     * Promise function (either using a string or a closure function)
     * This is an absolute requirement. Also the action should resolve the promise (using RSVP),
     * as detailed in the Ember guides linked above.
     *
     * @property promise
     * @type String|Function
     */
    promise: () => RSVP.resolve(),

    /**
     * Action to call when the promise is succesful
     * @property onsuccess
     * @type {Action}
     */
    onsuccess: () => {},

    /**
     * Action to call when the promise fails
     * @type {Action}
     */
    onerror: () => {},

    /**
     * If you are rendering the promise button as an inline component and not
     * in block form, set this to whatever text you want the button to display.
     * If you are rendering in block form, this does not apply.
     *
     * @property buttonText
     * @type {String}
     * @default ''
     */
    buttonText: '',

    /**
     * If you want a button-icon to be displayed, put any of the font-awesome icons
     * as a string here. Must use full icon name, including the class
     *
     * @property buttonIcon
     * @type {String}
     * @default ''
     */
    buttonIcon: '',

    /**
     * This is the style of the button. This corresponds to button states in Bootstrap.
     * Can be set to 'primary', 'default', 'success', 'danger', 'info', 'warning', or 'link'.
     * For more information on Bootstrap button states, [click here](https://getbootstrap.com/docs/4.0/components/buttons/).
     *
     * @property buttonStyle
     * @type {String}
     * @default 'primary'
     */
    buttonStyle: 'primary',

    /**
     * Title to display on button hover
     *
     * @property buttonTitle
     * @type {String}
     * @default null
     */

    /**
     * Sets the icon to be shown when the promise is resolving. See
     * [here](http://fontawesome.io/examples/#animated) for a list
     * of possible icons.
     *
     * @property spinIcon
     * @type {String}
     * @default 'fa-solid fa-rotate'
     */
    spinIcon: 'fa-solid fa-rotate',

    /**
     * Boolean value that tells whether or not the button's width needs to stay
     * constant regardless of state.
     *
     * @property autoWidth
     * @type {Boolean}
     * @default true
     */
    autoWidth: true,

    /**
     * External control for the disabled state of the button. This is different
     * than the regular `disabled` setting because the state of the button
     * will automatically modify whether or not the button is spinning based on
     * whether or not the promise is in the process of running.
     *
     * @property disableWhen
     * @type {Boolean}
     * @default false
     */
    disableWhen: false,

    /**
     * Holds the state of the button. Can be an empty object (no state, promise has
     * not been clicked), rejected (promse has rejected), or resolved (promise has resolved)
     *
     * @private
     * @property state
     * @type {Object}
     */
    state: {},

    /**
     * The time that the state of the button should be shown before returning to
     * default. Time must be specified in milliseconds.
     *
     * @property timeout
     * @type {Number}
     * @default 3000
     */
    timeout: 3000,

    /**
     * If true, show the green checkmark when the promise button is successful
     *
     * @property showSuccess
     * @type {Boolean}
     * @default true
     */
    showSuccess: true,

    /**
     * Sets the progress percent of the button. Used with submitting to show a sort of progress bar.
     * Must be a number between 0 and 100, setting to null disables it.
     *
     * @property progress
     * @type {Number}
     * @default null
     */
    progress: null,

    /**
     * Private instance variable holding whether or not the button is currently
     * submitting the promise. Used to modify the spinning state of `FwSpinButton`
     *
     * @private
     * @property submitting
     * @type {Boolean}
     * @default false
     */
    submitting: false,

    /**
     * Holds the Ember.run function responsible for resetting the state of a button
     * after the specified `timeout`.
     *
     * @private
     * @property showStateTimeout
     * @default null
     */
    showStateTimeout: null,

    /**
     * This returns the buttonStyle based on the state that the button is in.
     * If success, it is a success class; if error, danger class; and otherwise
     * it is the normal button class. Watches: buttonStyle, state.
     * @property buttonState
     * @type {Computed}
     * @private
     */
    buttonState: computed('buttonStyle', 'state', function () {
        let state = this.get('state');

        if (state.rejected) {
            return 'danger';
        } else if (state.resolved) {
            return 'success';
        } else {
            return this.get('buttonStyle');
        }
    }),

    /**
     * This returns a string of all the classes to be set on the main element.
     * Specifically the button state, joined to all the other classes. Watches: buttonState, class.
     * @property concatenatedClasses
     * @type {Computed}
     * @private
     */
    concatenatedClasses: computed('buttonState', 'class', function () {
        let classes = [this.get('class')];

        classes.push(`btn-${this.get('buttonState')}`);

        return classes.join(' ');
    }),

    _showState(state) {
        let timeout = this.get('timeout');
        // if we have a timeout, delay showing no state
        // if showSuccess is false though, do no show the check on success
        if (timeout && (!state.resolved || this.get('showSuccess'))) {
            this.setProperties({
                state,
                submitting: false,
                showStateTimeout: later(this, function () {
                    this.set('state', {});
                    this.set('showStateTimeout', null);
                }, timeout)
            });
        } else {
            // just set the state directly as we have no delay
            this.setProperties({state: {}, submitting: false});
        }
    },

    willDestroy() {
        this._super(...arguments);
        cancel(this.get('showStateTimeout'));
    },

    actions: {
        click(event) {
            if (!event) {
                event = window.event;
            }
            event.preventDefault();
            this.set('submitting', true);
            this.set('state', {});

            this.promise().then(() => {
                this._showState({resolved: true});
                this.onsuccess(...arguments);
            }).catch((error) => {
                this._showState({rejected: true});
                this.onerror(error);
                // do not rethrow the error as this button's job is to catch it, but log it so its not lost
                // TODO: log only in dev?
                if (!isNone(error)) {
                    // eslint-disable-next-line no-console
                    console.error(error);
                }
            });
        }
    }
});
