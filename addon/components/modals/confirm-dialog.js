import {computed} from '@ember/object';
import {assign} from '@ember/polyfills';

import BaseModal from '@bennerinformatics/ember-fw/components/modals/base';

import layout from '@bennerinformatics/ember-fw/templates/components/modals/confirm-dialog';

const buttonDefaults = {
    closeButtonText: 'Close',
    closeButtonStyle: 'danger',
    closeButtonIcon: 'fa-solid fa-xmark'
};
/**
 * Confirm Dialog is a modal very similar to [confirm-choice modal](Confirm-ChoiceModal) that is pre-made for you in ember-fw. The only difference really is that the confirm-dialog
 * modal is for an alert with only a close button, while the confirm-choice has a confirm button. As with the confirm-choice as well, confirm-dialog is made in such a way that you only
 * need to call it using FwFullscreenModal, rather than making your own modal following the instructions detailed in [BaseModal](BaseModal.html).
 * Because this is a regular modal, it will be called as a modal that you had created, and everything will be passed through the model property as usual. For more information on how to use the FwFullscreenModal
 * component, [see its documentation](FwFullscreenModal.html).
 *
 * Basic Usage Example:
 * ```handlebars
 * <FwFullscreenModal @modal='confirm-dialog' @size='md' @close={{action 'closeMyModal'}} @model={{hash
 *		   message="Your Data may be out of sync. Look into it"
 *		   ...
 *		}}
 * />
 * ```
 *
 * In order to pass in any data, you must pass in the data to the `model` attribute with the hash helper. Listed below in the different "properties" are actually not technically properties, but all sub-properties
 * of the model parameter in order to properly customize the confirm-choice modal.
 *
 * @class Confirm-DialogModal
 */
export default BaseModal.extend({
    layout,
    /**
     * This is the internal computed property for model that sets defaults for model if they remain unset by the end user.
     * @property _model
     * @type {Computed}
     * @private
     */
    _model: computed('model', function () {
        return assign({}, buttonDefaults, this.get('model'));
    })

    /**
     * This is the only property passed into the FwFullscreenModal component that actually transfers to this component, and it should be a hash of any of the other properties you wish to set.
     * @property model
     * @protected
     */

    /**
     * The message that will be displayed in the body of the modal.
     * @property model.message
     * @type {String}
     */
    /**
     * This is the CSS style for the close button (follows bootstrap btn classes). Most common options are: danger, primary, and default, but for the full list of options,
     * [see Bootstrap's documentation](https://getbootstrap.com/docs/4.0/components/buttons/#examples). (Note: btn- is added to this button style to make the full button class)
     * @property model.closeButtonStyle
     * @type {String}
     * @default 'danger'
     */
    /**
     * Text to appear on the close or cancel button.
     * @property model.closeButtonText
     * @type {String}
     * @default 'Close'
     */
    /**
      * Font-awesome icon to be displayed on the close button. For a full list of fontawesome icons, [click here](https://fontawesome.com/v5/search?m=free).
      * @property model.closeButtonIcon
      * @type {String}
      * @default 'fa-solid fa-xmark'
      */
});
