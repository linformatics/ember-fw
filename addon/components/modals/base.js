import Component from '@ember/component';
import {on} from '@ember/object/evented';
import {EKMixin, EKOnInsertMixin, keyUp} from 'ember-keyboard';

// This is a bit of a hack - needed so we can identify
// modal component objects
export const ModalComponentIdentifier = '__FW__MODAL__COMPONENT__';
/**
 * BaseModal contains the core functionality of the modal components for Ember FW. It should be used to extend for each of your modals in the following way:
 * ```javascript
 * import BaseModal from '@bennerinformatics/ember-fw/components/modals/base';
 *
 * export default BaseModal.extend({
 *  	//... regular component information goes here
 * });
 * ```
 * For a detailed introduction about modals in general, see our [Ember FW Documentation on Modals](https://linformatics.bitbucket.io/docs/addons/client/ember-fw/concepts/modals)
 * @class BaseModal
 */
export default Component.extend(EKMixin, EKOnInsertMixin, {
    [ModalComponentIdentifier]: true,

    tagName: 'article',
    classNames: 'fw-modal',

    /**
     * Action to call when closing the modal. This is set by whatever is passed to the [FwFullscreenModal](FwFullscreenModal.html) attribute `close`.
     * @property closeModal
     * @type {Action}
     */
    closeModal: () => {},

    /**
     * Action that can be called when confirming the modal. This is set to be whatever is passed into the [FwFullscreenModal](FwFullscreenModal.html) attribute `confirm`, though
     * often in modals this option is not used.
     * @property confirm
     * @type {action}
     */

    /**
      * Whatever data is passed into the model attribute on the [FwFullscreenModal](FwFullscreenModal.html) component can be accessed from both the handlebars and javascript
      * files under this variable name.
      * @property model
      * @type {Dynamic}
      */
    confirm: on(keyUp('Enter'), function () {
        this.send('confirm');
    }),

    cancel: on(keyUp('Escape'), function () {
        this.send('closeModal');
    }),

    actions: {
        confirm() {},

        closeModal() {
            this.closeModal();
        }
    }
});
