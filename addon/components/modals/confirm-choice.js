import {computed} from '@ember/object';
import {assign} from '@ember/polyfills';
import {inject} from '@ember/service';
import RSVP from 'rsvp';
import BaseModal from '@bennerinformatics/ember-fw/components/modals/base';
import {createErrorMessage} from '@bennerinformatics/ember-fw/utils/error';
import layout from '@bennerinformatics/ember-fw/templates/components/modals/confirm-choice';

const buttonDefaults = {
    confirmButtonText: 'Confirm',
    confirmButtonStyle: 'primary',
    confirmButtonIcon: 'fa-solid fa-check',
    closeButtonText: 'Cancel',
    closeButtonIcon: 'fa-solid fa-xmark'
};
/**
 * Confirm Choice is a modal that is pre-made for you in ember-fw. While the documentation for [FwFullscreenModal](FwFullscreenModal.html), suggested that you need to follow the basic instructions in
 * to make your own modal, as detailed in [BaseModal](BaseModal.html), confirm-choice is one that is already made with the design of simply confirming a message.
 * Because this is a regular modal, it will be called as a modal that you had created, and everything will be passed through the model property as usual. For more information on how to use the FwFullscreenModal
 * component, [see its documentation](FwFullscreenModal.html).
 *
 * Basic Usage Example:
 * ```handlebars
 * <FwFullscreenModal @modal='confirm-choice' @size='md' @close={{action 'closeMyModal'}} @model={{hash
 *		   message="Are you sure you want to do this?"
 *		   onConfirm=(action 'confirmMyModal')
 *		   confirmButtonText="Yes"
 *		   ...
 *		}}
 * />
 * ```
 *
 * In order to pass in any data, you must pass in the data to the `model` attribute with the hash helper. Listed below in the different "properties" are actually not technically properties, but all sub-properties
 * of the model parameter in order to properly customize the confirm-choice modal.
 * @class Confirm-ChoiceModal
 */
export default BaseModal.extend({
    layout,
    notifications: inject(),

    /**
     * This is the internal computed property for model that sets defaults for model if they remain unset by the end user.
     * @property _model
     * @type {Computed}
     * @private
     */
    _model: computed('model', function () {
        return assign({}, buttonDefaults, this.get('model'));
    }),

    /**
     * This is the only property passed into the FwFullscreenModal component that actually transfers to this component, and it should be a hash of any of the other properties you wish to set.
     * @property model
     * @protected
     */

    /**
     * The message that will be displayed in the body of the modal.
     * @property model.message
     * @type {String}
     */
    /**
     * This is the action to be called when the "confirm" button is hit in the modal. This only needs to be an action, which returns a promise, all error checking is handled by the modal itself.
     * @property model.onConfirm
     * @type {Action}
     */
    /**
     * This is the CSS style for the confirm button (follows bootstrap btn classes). Most common options are: danger, primary, and default, but for the full list of options,
     * [see Bootstrap's documentation](https://getbootstrap.com/docs/4.0/components/buttons/#examples). (Note: btn- is added to this button style to make the full button class)
     * @property model.confirmButtonStyle
     * @type {String}
     * @default 'primary'
     */
    /**
     * This is the text which will appear on the confirm button.
     * @property model.confirmButtonText
     * @type {String}
     * @default 'Confirm'
     */
    /**
     * Font-awesome icon to be displayed on the confirm button. For a full list of fontawesome icons, [click here](https://fontawesome.com/v5/search?m=free).
     * @property model.confirmButtonIcon
     * @type {String}
     * @default 'fa-solid fa-check'
     */
    /**
     * Text to appear on the close or cancel button.
     * @property model.closeButtonText
     * @type {String}
     * @default 'Close'
     */
    /**
      * Font-awesome icon to be displayed on the close button. For a full list of fontawesome icons, [click here](https://fontawesome.com/v5/search?m=free).
      * @property model.closeButtonIcon
      * @type {String}
      * @default 'fa-solid fa-xmark'
      */

    actions: {
        confirm() {
            if (this.get('model.onconfirm')) {
                return RSVP.resolve(this.get('model.onconfirm')()).then(() => {
                    this.closeModal();
                }).catch((error) => {
                    this.get('notifications').showError(createErrorMessage(error), 'modal-top', true);
                    throw error;
                });
            }
        }
    }
});
