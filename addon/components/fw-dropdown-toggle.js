import {isArray} from '@ember/array';
import Component from '@ember/component';
import {computed} from '@ember/object';
import layout from '../templates/components/fw-dropdown-toggle';
import {inject} from '@ember/service';

/**
 * This component is used internally by `fw-dropdown` as setting a tag based on a parameter is difficult otherwise
 * Minimal usage is simply as below. It may not have much usage outside of `fw-dropdown`.
 *
 * If the tag is set, the component will automatically update the attributes based on the type, so links are styled like links
 *
 * ```handlebars
 * <FwDropDownToggle @label="Dropdown" />
 * ```
 * @class FwDropdownToggle
 * @extends Ember.Component
 * @module Components
 */
export default Component.extend({
    layout,
    /**
     * The tagName property is often overridden in this component, so it is worth mentioning, though it is inherited by Components.
     * For documentation on this functionality, [click here](https://guides.emberjs.com/v2.18.0/components/customizing-a-components-element/#toc_customizing-the-element)
     * @property tagName
     * @type {String}
     * @default 'button'
     * @inherited
     */
    tagName: 'button',
    classNames: ['dropdown-toggle'],
    classNameBindings: ['_class', '_active:active'],
    attributeBindings: ['role', 'toggle:data-toggle'],

    router: inject(),

    // simply class computation
    /**
     * Button Class (from bootstrap) is computed for the component. Watches _style, tagName
     * @property _class
     * @type {Computed Property}
     * @return {String} Button Class
     * @private
     */
    _class: computed('tagName', '_style', function() {
        // default style to default if the tag is a button
        let style = this.get('style') || (this.get('tagName') === 'button' && 'default');
        if (style) {
            return `btn btn-${style}`;
        }

        return null;
    }),

    /**
     * Style of the component's button without the 'btn-' prefix. Should be one of the bootstrap styles, commonly: 'default', 'primary',
     * 'danger'. See [bootstrap docs](https://getbootstrap.com/docs/4.0/components/buttons/) for more info. If this is null, 'btn-default' class is used.
     * @property style
     * @type {String}
     * @default null
     */
    style: null,

    /**
     * Role bindings for the component.
     * @property role
     * @type {Computed Property}
     * @return {String} Role
     * @private
     */
    role: computed('tagName', function() {
        return this.get('tagName') === 'a' ? 'button' : null;
    }),

    /**
     * How the toggle is toggled
     * @property toggle
     * @type {String}
     * @default 'dropdown'
     */
    toggle: 'dropdown',

    /**
     * Label for the toggle
     * @property label
     * @type {String}
     * @default null
     */
    label: null,

    /**
     * Font Awesome default icon to use for the toggle. Must have icon name and prefix
     * @property icon
     * @type {String}
     * @default null
     * @public
     */
    icon: null,

    /**
     * Font Awesome icon to use for the toggle when open. Must have icon name and prefix
     * @property iconOpen
     * @type {String}
     * @default null
     */
    iconOpen: null,

    /**
     * Font Awesome icon to use for the button when closed. Must have icon name and prefix
     * @property iconClosed
     * @type {String}
     * @default null
     */
    iconClosed: null,

    /**
     * If currently viewing this route, the active class is added to the link.
     * @property active
     * @type {String}
     * @default null
     */
    active: null,

    /**
     * Computes whether the route is active based on the current route name and the active value. If it matches either, this is used
     * to make the text bold. Watches: active, router.currentRouteName
     * @property _active
     * @type {Computed Property}
     * @return {Boolean} Active
     * @private
     */
    _active: computed('active', 'router.currentRouteName', function() {
        let active = this.get('active');
        let router = this.get('router');
        // if given an array, any of those can be active
        if (isArray(active)) {
            return active.any((route) => router.isActive(route));
        }
        return router.isActive(active);
    })
});
