// This code all borrowed from https://github.com/runspired/liquid-fire-hooks
// Liquid-wormhole works a little differently so we needed to hijack this to work with modals

import {ModalComponentIdentifier} from '@bennerinformatics/ember-fw/components/modals/base';
import {get} from '@ember/object';
import {later} from '@ember/runloop';

function hasChild(component) {
    if (!component) {
        return false;
    }

    return component.childViews && component.childViews.length;
}

function getFirstChild(component) {
    return hasChild(component) ? component.childViews[0] : null;
}

function isComponent(component) {
    return get(component, ModalComponentIdentifier);
}

function componentFinder(lfWrapper) {
    let maxDepth = 6;
    let curDepth = 0;
    let component = getFirstChild(lfWrapper);

    while (!isComponent(component) && (curDepth++ <= maxDepth) && hasChild(component)) {
        component = getFirstChild(component);
    }

    return isComponent(component) ? component : null;
}

export default function withHookTransition(delegateTo, ...args) {
    return this.lookup(delegateTo).apply(this, args).then((infos) => {
        if (this.newView) {
            let component = componentFinder(this.newView.childViews[0].notify);

            if (component && component.onDisplay) {
                later(component, component.onDisplay, 150);
            }
        }

        return infos;
    });
}
