import {get} from '@ember/object';
import {inject as injectService} from '@ember/service';
import AjaxService from 'ember-ajax/services/ajax';
import {AjaxError, isAjaxError} from 'ember-ajax/errors';
import {isNone} from '@ember/utils';

export function SetupError(error) {
    AjaxError.call(this, error, 'API request failed due to a server setup issue');
}

SetupError.prototype = Object.create(AjaxError.prototype);

export function isSetupError(errorOrStatus, payload) {
    if (isAjaxError(errorOrStatus)) {
        return errorOrStatus instanceof SetupError;
    } else {
        return get(payload || {}, 'type') === 'SetupError';
    }
}

const JSONContentType = 'application/json';

function isJSONContentType(header) {
    if (!header || isNone(header)) {
        return false;
    }

    return header.indexOf(JSONContentType) === 0;
}

/**
 * Ajax is a service that is used to make direct ajax requests to the serverside from the client without using Ember's built in requests, such as save, browse, etc. There isn't much difference between this and Ember Ajax Service, which it extends.
 * For full documentation, [click here](https://github.com/ember-cli/ember-ajax#ember-ajax). One of the main differences between Ember Ajax and FW Ajax is that FW ajax is designed to work with the [config service](ConfigService.html) to automatically make
 * the URL for you, and FW Ajax sets the content-type to JSON automatically.
 *
 * The main difference between Ember-Ajax and Ember-FW Ajax is that we added an ability to force a `SetupError` to display a notification to the global outlet (see Notifications).
 * Additionally, we always stringify the data passed to the server as JSON (so that you don't need to do that yourself), since our Serverside expects JSON data. This does not change how you
 * use the ajax service at all, so there is no need to document each function we slightly tweaked. In each case, `this._super(...arguments);` is called as well.
 *
 * This service is also extended by Ember FW GC, so if you intend to use the AjaxService, you should probably double check its documentation as well [here](https://linformatics.bitbucket.io/docs/addons/client-api/ember-fw-gc/classes/AjaxService.html).
 *
 * For a basic example of how to implement the config (and ajax) service at the basic level, see our
 * [Conceptual Introduction - Advanced Topics](https://linformatics.bitbucket.io/docs/training/intro/advanced/#ajax-requests).
 *
 * @module Services
 * @class AjaxService
 * @extends Ember Ajax Service
 */

const ajaxService = AjaxService.extend({
    notifications: injectService(),

    handleResponse(status, headers, payload) {
        if (this.isSetupError(status, headers, payload)) {
            this.get('notifications').showError(payload.message, 'global');
            return new SetupError(payload);
        }

        return this._super(...arguments);
    },

    _makeRequest(hash) {
        if (isJSONContentType(hash.contentType) && hash.type !== 'GET') {
            if (typeof hash.data === 'object') {
                hash.data = JSON.stringify(hash.data);
            }
        }

        return this._super(...arguments);
    },

    isSetupError(status, headers, payload) {
        return isSetupError(status, payload);
    }
});

ajaxService.reopen({
    contentType: JSONContentType
});

export default ajaxService;
