import Service from '@ember/service';
import Evented from '@ember/object/evented';
import {A as emberA} from '@ember/array';

/**
 * This service can be injected in the following way:
 *
 * ```javascript
 * import Route from '@ember/routing/route';
 * import {inject as service} from '@ember/service';
 *
 * export default Route.extend({
 *     notifications: service(), // notifications will be available as a property
 *
 *     // ... your other code
 * });
 * ```
 *
 * Then described below are the various methods that you are able to call from the Notifications Service once it is injected.
 *
 * @class NotificationsService
 * @extends Ember.Service
 * @uses Ember.Evented
 * @module Services
 */
export default Service.extend(Evented, {
    /**
     * All of the notifications currently in the DOM. Internal only.
     * @private
     * @property content
     * @type {Array}
     */
    content: emberA(),

    /**
     * Shows an informational message, ie it shows a message to the selected outlet with the "info" styling.
     *
     * @method showInfo
     * @param  {String} message     Message to show in the notification.
     * @param  {String} outlet      Which outlet to render the notification into
     * @param  {Boolean|String} dismissable If boolean, whether or not the notification will disappear on its own. If string, length of time before it disappears.
     */
    showInfo(message, outlet, dismissable) {
        this.handleNotification({
            type: 'info',
            message,
            outlet,
            dismissable
        });
    },

    /**
     * Shows a warning message, ie it shows a message to the selected outlet with the "warning" styling.
     *
     * @method showWarning
     * @param  {String} message     Message to show in the notification.
     * @param  {String} outlet      Which outlet to render the notification into
     * @param  {Boolean|String} dismissable If boolean, whether or not the notification will disappear on its own. If string, length of time before it disappears.
     */
    showWarning(message, outlet, dismissable) {
        this.handleNotification({
            type: 'warning',
            message,
            outlet,
            dismissable
        });
    },

    /**
     * Shows an error message, ie it shows a message to the selected outlet with the "error" styling.
     *
     * @method showError
     * @param  {String} message     Message to show in the notification.
     * @param  {String} outlet      Which outlet to render the notification into
     * @param  {Boolean|String} dismissable If boolean, whether or not the notification will disappear on its own. If string, length of time before it disappears.
     */
    showError(message, outlet, dismissable) {
        this.handleNotification({
            type: 'danger',
            message,
            outlet,
            dismissable
        });
    },

    /**
     * Shows a success message
     *
     * @method showSuccess
     * @param  {String} message     Message to show in the notification.
     * @param  {String} outlet      Which outlet to render the notification into
     * @param  {Boolean|String} dismissable If boolean, whether or not the notification will disappear on its own. If string, length of time before it disappears.
     */
    showSuccess(message, outlet, dismissable) {
        this.handleNotification({
            type: 'success',
            message,
            outlet,
            dismissable
        });
    },

    /**
     * Internal method that handles a raw notification. This is used by all of the show____ methods, so most of the time it is best just to use the proper function to build the notification for you.
     *
     * @private
     * @method handleNotification
     * @param  {Object} message options for the notification
     */
    handleNotification(message) {
        message.type = message.type || 'info';
        message.outlet = message.outlet || 'default';

        // if the user passed in a string of time in seconds, honor that.
        if (typeof(message.dismissable) == 'string')  {
            message.length = message.dismissable;
            message.dismissable = true;
        } else if (message.dismissable !== true) {
            delete message.dismissable;
        }

        if (message.type === 'error') {
            message.type = 'danger';
        }

        this.get('content').pushObject(message);
    },

    /**
     * Close a notification given by a particular object.
     *
     * @method closeNotification
     * @param  {Object} message notification object
     */
    closeNotification(message) {
        this.get('content').removeObject(message);
    },

    /**
     * Close all notifications for a particular outlet.
     *
     * @method closeAllNotifications
     * @param  {String} outlet outlet to close notifications in
     */
    closeAllNotifications(outlet) {
        if (outlet) {
            this.set('content', this.get('content').filter((item) => {
                return item.outlet !== outlet;
            }));
        } else {
            this.get('content').clear();
        }
    }
});
