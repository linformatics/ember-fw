import Service from '@ember/service';
import {computed} from '@ember/object';

/**
 * This service packages important configuration properties into a Service that can be injected anywhere that is needed. The config service is primarily used in tandem with the ajax service to make direct network
 * requests to the serverside without going through the Ember model. The config service is responsible for creating the Ajax url which will be used by the Ajax Service.
 *
 * This config service is extended by our ember-fw-gc service called config, which adds a few more functions, so if you are using ember-fw-gc, check out its documentation for the ember-fw-gc config service,
 * [here](https://linformatics.bitbucket.io/docs/addons/client-api/ember-fw-gc/classes/ConfigService.html).
 *
 * For a basic example of how to implement the config (and ajax) service at the basic level, see our
 * [Conceptual Introduction - Advanced Topics](https://linformatics.bitbucket.io/docs/training/intro/advanced/#ajax-requests).
 *
 * @class ConfigService
 * @extends Ember.Service
 * @module Services
 */
export default Service.extend({

    /**
     * This is a computed property which returns window.location.pathname, and is used in forming the url correctly.
     * Because we are using Ember's HashLocation, we can reliably use window.location.pathname to determine the root
     * url, rather than relying on rewriting it during app install.
     * @property url
     * @type Computed
     */
    url: computed(function () {
        return window.location.pathname;
    }),

    /**
     * Computed property that refers to the api rool url
     * Used to make api requests
     *
     * @property apiRoot
     * @type Computed
     */
    apiRoot: computed('api', 'url', function () {
        return this.get('url') + this.get('api');
    }),

    /**
     * This is an internal function which is called by formUrl.
     *
     * @method _formUrl
     * @private
     * @return {String}
     */
    _formUrl(base, args) {
        let queries = [];
        let key, url, queryObject;

        if (args[args.length - 1] instanceof Object) {
            queryObject = args.pop();
            for (key in queryObject) {
                if (queryObject.hasOwnProperty(key)) {
                    queries.push(`${key}=${encodeURIComponent(queryObject[key])}`);
                }
            }
        }

        url = `${base}/${args.join('/')}/`;

        if (queries.length) {
            url += `?${queries.join('&')}`;
        }

        return url;
    },

    /**
     * Forms a url from the arguments specified.
     *
     * Expects arguments as strings, with the exception of the last
     * argument, which can be an object of properties to convert
     * to query parameters.
     *
     * Example usage:
     * ```javascript
     * // assuming apiRoot is '/api/'
     * let id = 10
     * let url = config.formUrl('users', id, {include: 'posts'});
     *
     * // url is '/api/users/10/?include=posts'
     * ```
     *
     * @method formUrl
     * @return {String}
     */
    formUrl() {
        let args = Array.prototype.slice.call(arguments);
        return this._formUrl(this.get('apiRoot'), args);
    }
});
