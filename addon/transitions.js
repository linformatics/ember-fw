// used by liquid-fire for transitions
export default function () {
    this.transition(
        this.hasClass('fullscreen-modal-container'),
        this.toValue(true),
        this.use('modal-hook', 'fade', {duration: 150}),
        this.reverse('explode', {
            pick: '.fullscreen-modal',
            use: ['fade', {duration: 80}]
        }, {
            use: ['fade', {duration: 150}]
        })
    );
}
