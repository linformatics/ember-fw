import Mixin from '@ember/object/mixin';

/**
 * Mixin that enables tooltip function
 *
 * @class Tooltip
 * @extends Ember.Mixin
 * @module Mixins
 */
export default Mixin.create({
    /**
     * Placement of the popover (top, bottom, left, or right)
     * @property placement
     * @type {String}
     * @default 'top'
     */
    placement: 'top',

    /**
     * text of the popover
     * @property text
     * @type {String}
     */
    text: '',

    /**
     * Toggle for the tooltip
     * @property toggle
     * @type {String}
     * @default 'tooltip'
     */

    toggle: 'tooltip',

    attributeBindings: [
        'placement:data-placement',
        'toggle:data-toggle'
    ],

    didInsertElement() {
        this._super(...arguments);
        this.$().tooltip({
            title: this.get('text'),
            container: 'body'
        });
    },

    willDestroyElement() {
        this._super(...arguments);
        this.$().tooltip('destroy');
    }
});
