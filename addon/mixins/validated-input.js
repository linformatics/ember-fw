import Mixin from '@ember/object/mixin';
import {computed, defineProperty} from '@ember/object';
import {reads, alias, not, and} from '@ember/object/computed';

/**
 * Mixin that adds functionality to validation-enabled components. Each of those components use this mixin, and because of this, this mixin is mostly internal to ember-fw. In the
 * app scenario, you should be mostly using the components that are already defined rather than making your own component and using this mixin, but if the situation arises, the
 * following import statement would be used:
 * ```javascript
 * import ValidatedInputMixin from '@bennerinformatics/ember-fw/mixins/validated-input';
 *
 * export default Component.extend(ValidatedInputMixin, {
 *      // ... your component content here
 * });
 * ```
 * Then all of the properties in this mixin would be available for use in that component.
 * @class ValidatedInputMixin
 * @module Mixins
 */
export default Mixin.create({
    /**
     * Key being validated by this field
     * @property valuePath
     * @type {String}
     */
    valuePath: '',

    /**
     * Action to call when the input changes
     * @property update
     * @type {Action}
     */
    update: () => {},

    /**
     * Action to call when clicking out of the text field
     * @property focus-out
     * @type {Action}
     */
    'focus-out': () => {},

    /**
     * Model containing the validations
     * @property model
     * @type {DS.Model}
     */
    model: null,

    /**
     * If true, validation style classes will be skipped
     * @property disableStyle
     * @type {Boolean}
     * @default false
     */
    disableStyle: false,

    /**
     * Current value of the object
     * @private
     * @property validation
     */
    validation: null,
    /**
     * Current value of the object
     * @private
     * @property value
     */
    value: null,

    didReceiveAttrs() {
        this._super(...arguments);

        let valuePath = this.get('valuePath');

        defineProperty(this, 'validation', reads(`model.validations.attrs.${valuePath}`));
        defineProperty(this, 'value', alias(`model.${valuePath}`));
        defineProperty(this, 'didValidate', computed('model.hasValidated.[]', function () {
            return this.get('model.hasValidated').includes(valuePath);
        }));
    },

    concatenatedClasses: computed('classes', function () {
        let classes = [this.get('classes')];
        classes.push('form-control');
        return classes.join(' ');
    }),

    /**
     * This is an alias of when validation.isValidating is false.
     * @property notValidating
     * @type {Computed}
     * @readonly
     */
    notValidating: not('validation.isValidating'),
    /**
     * This is an alias that tells us when the validation has completed and is resolved successfully. It is an alias of when validation.isValid, didValidate, and notValidating
     * are all true.
     * @property isValid
     * @type {Computed}
     * @readonly
     */
    isValid: and('validation.isValid', 'notValidating', 'didValidate'),
    /**
     *
     * @property isInvalid
     * @type {Computed}
     * @readonly
     */
    isInvalid: reads('validation.isInvalid'),
    /**
     * This is when the validation has completed checking, but there is an error (ie validations resolved to false). It is an alias of notValidating, showErrorMessage, and validation
     * all of which must be true.
     * @property hasError
     * @type {Computed}
     * @readonly
     */
    hasError: and('notValidating', 'showErrorMessage', 'validation'),
    /**
     * This is an alias of when validation.isValidating is false. Essentially just an internal property for ease of access.
     * @property notValidating
     * @type {Computed}
     * @readonly
     */
    showErrorMessage: and('didValidate', 'isInvalid', 'notValidating'),

    /**
     * Determines the current visible state of the field
     * @property state
     * @type {Computed}
     */
    state: computed('isValid', 'hasError', 'didValidate', 'disableStyle', function () {
        if (!this.get('didValidate') || this.get('disableStyle')) {
            return '';
        }

        if (this.get('hasError')) {
            return 'has-error';
        }

        if (this.get('isValid')) {
            return 'has-success';
        }
        return '';
    }),

    actions: {
        /**
         * Updates the validations for the field then redirects to the update property
         * @param  {mixed} value New value
         */
        update(value) {
            let valuePath = this.get('valuePath');

            this.get('model.hasValidated').removeObject(valuePath);
            this.update(value);
        },

        /**
         * Action to call when deselecting the text field
         */
        focusOut() {
            let valuePath = this.get('valuePath');
            let model = this.get('model');

            if (!model) {
                return;
            }

            this.get('model').validate({on: [valuePath]}).finally(() => {
                this.get('model.hasValidated').pushObject(valuePath);

                this.get('focus-out')();
            });
        },

        /** Combination of {@link update} and {@link focusOut} to use for non-blurable fields */
        updateValidate(value) {
            let valuePath = this.get('valuePath');

            this.get('model.hasValidated').removeObject(valuePath);
            this.update(value);
            this.get('model').validate({on: [valuePath]}).finally(() => {
                this.get('model.hasValidated').pushObject(valuePath);
            });
        }
    }
});
