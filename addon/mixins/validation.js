import Mixin from '@ember/object/mixin';
import {A as emberA} from '@ember/array';
import RSVP from 'rsvp';

const {resolve, reject} = RSVP;

/**
 * Validation mixin that applies to models that use the ember-cp-validations mixin. This should be imported to the model file of the model you wish to validate in the following way:
 * ```javascript
 * import ChannelValidation from 'msgc/mixins/validations/channel';
 * import ValidationMixin from '@bennerinformatics/ember-fw/mixins/validation';
 *
 * export default Model.extend(ChannelValidation, ValidationMixin, {
 *     // ... your model content here
 * });
 * ```
 *
 * @class ValidationMixin
 * @module Mixins
 */
export default Mixin.create({
    hasValidated: null,

    init() {
        this._super(...arguments);

        this.set('hasValidated', emberA());
    },

    validate() {
        return this.get('validations').validate(...arguments).then(({validations}) => {
            this.get('hasValidated').pushObjects(validations.get('content').getEach('attribute'));

            return (validations.get('isValid')) ? resolve() : reject();
        });
    },

    save(options) {
        let {_super} = this;

        options = options || {};

        return this.validate().then(() => {
            return _super.call(this, options);
        });
    },

    clearValidations() {
        this.get('hasValidated').clear();
    }
});
