import Mixin from '@ember/object/mixin';
import {isHTMLSafe} from '@ember/string';
import $ from 'jquery';

/**
 * Mixin that enables popover functionality
 *
 * @class PopoverMixin
 * @extends Ember.Mixin
 * @module Mixins
 */
export default Mixin.create({
    /**
     * Placement of the popover (top, bottom, left, right, or auto)
     * auto is a special placement which will appear top or bottom based on location on the screen
     * @property placement
     * @type {String}
     * @default 'top'
     */
    placement: 'top',

    /**
     * Title of the popover
     * @property title
     * @type {String}
     */
    title: '',

    /**
     * Content of the popover
     * @property content
     * @type {String}
     */
    content: '',

    /**
     * Container for the popover, set to 'body' to prevent parent styling from modifying the popover
     * @property container
     * @type {String}
     * @default 'body'
     */
    container: 'body',

    /**
     * Trigger of the popover (hover, click, focus, or pinnable)
     * pinnable is special behavior which supports both click and hover
     * @property popoverTrigger
     * @type {String}
     * @default 'hover'
     */
    popoverTrigger: 'hover',

    /**
     * This is the toggle type for the popover. In most cases it should remain 'popover'
     * @property toggle
     * @type {String}
     * @default 'popover'
     */
    toggle: 'popover',

    attributeBindings: [
        'placement:data-placement',
        'toggle:data-toggle'
    ],

    _pinned: false,
    _reshow: false,

    didInsertElement() {
        this._super(...arguments);
        let self = this;

        // trigger might be pinnable, which allows both click and hover
        let trigger = this.get('popoverTrigger');
        // placement could be auto, which does top or bottom based on location
        let placement = this.get('placement');
        if (placement === 'auto') {
            placement = function(context, source) {
                let offset = $(source).offset().top - $(window).scrollTop();
                return offset < 100 ? 'bottom' : 'top';
            };
        }

        // if the content is html safe, render it as html
        let content = this.get('content');
        let html = false;
        if (isHTMLSafe(content)) {
            content = content.toHTML();
            html = true;
        }

        // define the actual popover
        let jq = this.$();
        let popover = jq.popover({
            title: this.get('title'),
            content,
            html,
            trigger: trigger == 'pinnable' ? 'manual' : trigger,
            container: this.get('container'),
            placement
        });

        // pinnable logic
        if (trigger === 'pinnable') {
            popover.click((e) => {
                // stop the click from propagating
                e.stopPropagation();
                e.preventDefault();

                // when clicking, toggle the state
                self.toggleProperty('_pinned');
                // if toggleing to not pinned, remove it to be a bit friendlier
                if (!self.get('_pinned')) {
                    jq.popover('hide');
                    self.set('_reshow', true);
                // but after the first time allow reshowing on click
                } else if (self.get('_reshow')) {
                    jq.popover('show');
                }
            }).mouseenter(() => {
                // show the popover
                if (!self.get('_pinned')) {
                    jq.popover('show');
                }
            }).mouseleave(() => {
                // hide the popover
                if (!self.get('_pinned')) {
                    jq.popover('hide');
                    self.set('_reshow', false);
                }
            });
        }
    },

    willDestroyElement() {
        this._super(...arguments);
        this.$().popover('destroy');
    }
});
