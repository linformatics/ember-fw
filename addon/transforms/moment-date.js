/* global moment */
import Transform from 'ember-data/transform';

/**
 * Moment-date is a custom transform for date objects in ember Models. When it serializes it turns it into a moment object, and will even format it according to the default format
 * passed in to it, and when it sends it to the serverside it will format it correctly for saving on the serverside (ie 1970-01-01 00:00:00).
 *
 * It is very simple to use, and it is only used in the models (as any other transform would be), for example:
 * ```javascript
 *  // make sure you have the following line at the top: / * global moment * / (close the spaces between the / and *)
 *  import Model from 'ember-data/model';
 *  import attr from 'ember-data/attr';
 *
 *  export default Model.extend({
 *      startDate: attr('moment-date'),
 *      //optionally you may also use the format attribute to format it automatically as a string
 *      endDate: attr('moment-date', {format: 'MM-DD-YYYY'}),
 *      //... your content here...
 *  });
 *  ```
 *  For information on how to write the string format, check out [Moment docs](https://momentjs.com/docs/#/displaying/format/)
 * @class MomentDateTransform
 * @module Miscellaneous
 */
export default Transform.extend({
    deserialize(serialized, {format} = {}) {
        if (serialized) {
            return moment(serialized, format);
        }
        return serialized;
    },

    serialize(deserialized, {format} = {}) {
        if (deserialized) {
            if (!(deserialized instanceof moment)) {
                // Deserialized is not a moment object, so make it one
                deserialized = moment(deserialized);
            }

            return deserialized.format(format || 'YYYY-MM-DD HH:mm:ss');
        }
        return deserialized;
    }
});
