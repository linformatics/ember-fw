/* Below is the main documentation for the addon. This file is only for documentation, it doesn't actually do anything. */
/**
 * Ember-FW is an assortment of various components and utilities that are used throughout all the Framework applications.
 * It is designed to augment the core ember framework and make many custom components and helpers to make all ember applications
 * look very similar. Unlike other Informatics addons, this addon does not have one main functionality that it is trying to
 * accomplish, but just adds many critical functionalities used throughout the Informatics Apps.
 *
 * This documentation covers the various aspects of ember-fw, and gives you the API docs for how to properly use them. If you want more
 * conceptual documentation for Ember FW, see our [Ember FW docs](https://linformatics.bitbucket.io/docs/addons/client/ember-fw). It may be difficult
 * to find exactly what you are looking for due to the fact that all the items are in the generic category of "class," but for ease of access,
 * modules have been formed based on the different types of Ember structures you might need in Ember. If you know the exact name of the class
 * you are looking for, there is a search bar on the top left corner of the screen for ease of access. Or you can always click "s" to search and
 * use the up and down arrow keys for results.
 *
 * @module Introduction
 * @main Introduction
 */

/**
 * Components defined by ember-fw.
 *
 * @module Components
 * @main Components
 */

/**
 * Helpers defined by ember-fw.
 *
 * @module Helpers
 * @main Helpers
 */

/**
 * Mixins defined in ember-fw.
 *
 * @module Mixins
 * @main Mixins
 */

/**
 * Util files defined in ember-fw.
 *
 * @module Utils
 * @main Utils
 */
/**
 * Services defined in ember-fw.
 *
 * @module Services
 * @main Services
 */
/**
 * This module is just a catch all module. Rather than have many different modules with one class each, we decided to combine all of the modules that
 * would have had only a few classes into the Miscellaneous module. So this includes Routes, and Transforms.
 *
 * @module Miscellaneous
 * @main Miscellaneous
 */
