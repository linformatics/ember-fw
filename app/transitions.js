import emberFwTransitions from '@bennerinformatics/ember-fw/transitions';

// used by liquid-fire for transitions
export default function () {
    emberFwTransitions.apply(this, arguments);
}
