import Route from '@ember/routing/route';

// catch-all route for 404 errors
export default Route.extend({
    beforeModel() {
        return this.transitionTo('index');
    }
});
