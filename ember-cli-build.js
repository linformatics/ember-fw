/* eslint-env node */
/* eslint-disable no-var */
var EmberAddon = require('ember-cli/lib/broccoli/ember-addon');
var nodeSass = require('node-sass');

module.exports = function (defaults) {
    var app = new EmberAddon(defaults, {
        fingerprint: {enabled: false},
        sassOptions: {
            implementation: nodeSass
        }
    });

    /*
        This build file specifes the options for the dummy test app of this
        addon, located in `/tests/dummy`
        This build file does *not* influence how the addon or the app using it
        behave. You most likely want to be modifying `./index.js` or app's build file
    */
    app.import('node_modules/moment/moment.js');

    return app.toTree();
};
