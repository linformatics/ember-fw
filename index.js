/* eslint-env node */
/* eslint-disable no-var,object-shorthand,prefer-template */

'use strict';

module.exports = {
    name: '@bennerinformatics/ember-fw',
    included: function (app) {
        this._super.included.apply(this, arguments);

        this.importBootstrapJS(app);
        this.importFontawesomeFonts(app);
    },

    importBootstrapJS: function (app) {
        var bootstrapJs = 'node_modules/bootstrap-sass/assets/javascripts/bootstrap/';

        app.import(bootstrapJs + 'alert.js');
        app.import(bootstrapJs + 'dropdown.js');
        app.import(bootstrapJs + 'tooltip.js');
        app.import(bootstrapJs + 'popover.js');
    },
    importFontawesomeFonts: function(app) {
        var path = 'node_modules/@fortawesome/fontawesome-free/webfonts/';
        var dest = {destDir: 'assets/fontawesome'};

        // there are a lot of fonts and extensions with the same format, so just 2D loop them
        var exts = ['eot', 'svg', 'ttf', 'woff', 'woff2'];
        var fonts = ['brands-400', 'regular-400', 'solid-900'];
        for (var font of fonts) {
            for (var ext of exts) {
                app.import(path + 'fa-' + font + '.' + ext, dest);
            }
        }
    }
};
