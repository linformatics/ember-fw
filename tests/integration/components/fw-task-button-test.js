/* jshint expr:true */
import {expect} from 'chai';
import {describeComponent, it} from 'ember-mocha';
import hbs from 'htmlbars-inline-precompile';

describeComponent(
    'fw-task-button',
    'Integration: Component: fw-task-button',
    {
        integration: true
    },
    function() {
        it('renders', function() {
            // Set any properties with this.set('myProperty', 'value');
            // Handle any actions with this.on('myAction', function(val) { ... });
            // Template block usage:
            // this.render(hbs`
            //   <FwTaskButton>
            //     template content
            //   </FwTaskButton>
            // `);

            this.render(hbs`<FwTaskButton />`);
            expect(this.$()).to.have.length(1);
        });
    }
);
