/* eslint-env node */

module.exports = {
    command: 'ember test --filter="ESLint" -i',
    useYarn: true,
    scenarios: [
        {name: 'default'},
        {name: 'ember-release'},
        {name: 'ember-beta'},
        {name: 'ember-canary'}
    ]
};
